'use strict';

const reportModel = require('../../dbs/mongoManager').BulkNotifyModel;
const serviceLogger = require('../logger').serviceLogger;


/**
 * Create bulk notify report and save it mongoDb
 * @param {Object} params
 * @param {Object} devices
 * @param {Object} result
 */
module.exports = function (params, devices, result) {

    let androidRawReport, iosRawReport;
    if (result.android) {
        androidRawReport = result.android.android_sender_report;
    }
    if (result.ios) {
        iosRawReport = result.ios.ios_sender_report;
    }
    let successAndroid = 0;
    let failureAndroid = 0;
    let successIos = 0;
    let failureIos = 0;
    const iosResults = [];
    const androidResults = [];
    if (androidRawReport) {
        for (let andRep of  androidRawReport) {
            if (andRep.success) {
                successAndroid += andRep.success;
            }
            if (andRep.failure) {
                failureAndroid += andRep.failure;
            }
            let andRes = andRep.results;
            for (let pushRes of andRes) {
                let deviceData = devices.find(function (element, index, array) {
                    return element.device_token + "" === pushRes.device + "";
                });
                if (deviceData) {
                    let userId = deviceData.client_id ? deviceData.client_id : deviceData.worker_id;
                    if (userId) {
                        let val = {
                            user_id: userId,
                            token: pushRes.device,
                            error: pushRes.error ? pushRes.error : '',
                            result: pushRes.error ? 0 : 1
                        };
                        androidResults.push(val);
                        let deleteIndex = devices.findIndex((element, index, array) => {
                            return element.device_token + "" === pushRes.device + "";
                        });
                        if (deleteIndex !== -1) {
                            devices.splice(deleteIndex, 1);
                        }
                    }
                }

            }
        }
    }


    if (iosRawReport) {
        for (let iosRep of iosRawReport) {
            if (Array.isArray(iosRep.sent)) {
                successIos += iosRep.sent.length;
                let iosSendRes = iosRep.sent;
                for (let pushRes of iosSendRes) {
                    let deviceData = devices.find(function (element, index, array) {
                        return element.device_token + "" === pushRes.device + "";
                    });
                    if (deviceData) {
                        let userId = deviceData.client_id ? deviceData.client_id : deviceData.worker_id;
                        if (userId) {
                            let val = {
                                user_id: userId,
                                token: pushRes.device,
                                error: '',
                                result: 1
                            };
                            iosResults.push(val);
                            let deleteIndex = devices.findIndex((element, index, array) => {
                                return element.device_token + "" === pushRes.device + "";
                            });
                            if (deleteIndex !== -1) {
                                devices.splice(deleteIndex, 1);
                            }
                        }
                    }
                }
            }
            if (Array.isArray(iosRep.failed)) {
                failureIos += iosRep.failed.length;
                let iosFailRes = iosRep.failed;
                for (let pushRes of iosFailRes) {
                    let deviceData = devices.find(function (element, index, array) {
                        return element.device_token + "" === pushRes.device + "";
                    });
                    if (deviceData) {
                        let userId = deviceData.client_id ? deviceData.client_id : deviceData.worker_id;
                        if (userId) {
                            let errorMessage = pushRes.response && pushRes.response.reason ? pushRes.response.reason : 'BadDeviceToken';
                            let val = {
                                user_id: userId,
                                token: pushRes.device,
                                error: errorMessage,
                                result: 0
                            };
                            iosResults.push(val);
                            let deleteIndex = devices.findIndex((element, index, array) => {
                                return element.device_token + "" === pushRes.device + "";
                            });
                            if (deleteIndex !== -1) {
                                devices.splice(deleteIndex, 1);
                            }
                        }
                    }
                }
            }
        }
    }

    let reportObj = {
        notification_id: params.notification_id,
        tenant_id: params.tenant_id,
        city_ids: params.city_ids,
        app_id: params.app_id,
        client_type: params.client_type,
        client_ids: params.client_ids,
        title: params.title,
        message: params.message,
        created_at: params.created_at,
        report: {
            ios: {
                success: successIos,
                failure: failureIos,
                results: iosResults
            },
            android: {
                success: successAndroid,
                failure: failureAndroid,
                results: androidResults
            }
        }
    };

    const report = new reportModel(reportObj);
    report.save((err) => {
        if (err) {
            serviceLogger('error', `bulkNotifyReport->chatMessage.save->Error: ${err.message}`);
        } else {
            serviceLogger('info', `bulkNotifyReport->Bulk notify report successful created`);
            serviceLogger('info', `${JSON.stringify(params)}`)
        }
    });
};


