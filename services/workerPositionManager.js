'use strict';

const positionInfo = {
    simple_taxi_driver: {
        position_id: 1,
        name: 'Simple taxi driver',
        module_id: 1,
        has_car: 1
    },
    simple_truck_driver: {
        position_id: 2,
        name: 'Simple truck driver',
        module_id: 2,
        has_car: 1
    },
    simple_courier: {
        position_id: 3,
        name: 'Simple courier',
        module_id: 3,
        has_car: 0
    },
    simple_master: {
        position_id: 4,
        name: 'Simple master',
        module_id: 4,
        has_car: 0
    },
    courier_on_moto: {
        position_id: 5,
        name: 'Courier on moto',
        module_id: 3,
        has_car: 0
    },
    courier_on_auto: {
        position_id: 6,
        name: 'Courier on auto',
        module_id: 3,
        has_car: 0
    },
};


/**
 * Get position by id
 * @param {Number} positionId
 * @return {*}
 */
function getPositionById(positionId) {
    let resultPosition = null;
    Object.keys(positionInfo).forEach(key => {
        let position = positionInfo[key];
        if (positionId === position.position_id) {
            resultPosition = position;
        }
    });
    return resultPosition;
}


/**
 * Does position has car
 * @param {Number} positionId
 * @returns {boolean}
 */
function positionHasCar(positionId) {
    const position = this.getPositionById(positionId);
    return position && position.has_car === 1;

}
exports.positionInfo = positionInfo;
exports.getPositionById = getPositionById;
exports.positionHasCar = positionHasCar;