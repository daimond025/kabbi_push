'use strict';
const lib = require('./lib');


const properties = [
    'address',
    'date',
    'time',
    'tariff',
    'price',
    'currency'
];


/**
 * Replace values at template
 * @param {string} template
 * @param {object} values
 * {date:'17:48 22.02.12', price:'100',time:'12:22','address':'кирова 22','tariff':'эконом','currency':'RUB'}
 * @returns {*}
 */
function replace(template, values) {
    for (let property of properties) {
        let oldProperty = `#${property.toUpperCase()}#`;
        if (template.indexOf(oldProperty) !== -1 && values[property]) {
            template = lib.replaceString(oldProperty, values[property], template);
        }
    }
    return template;
}

exports.replace = replace;
exports.properties = properties;