'use strict';

exports.getConfig = function () {
    return require('dotenv-safe').load().parsed;
};

exports.getServiceVersion = () => {
    return require('../package.json').version;
};

exports.isString = function (val) {
    return typeof val === 'string' || ((!!val && typeof val === 'object') && Object.prototype.toString.call(val) === '[object String]');
};

exports.replaceString = function (oldS, newS, fullS) {
    return fullS.split(oldS).join(newS);
};




