'use strict';

const iosPush = require("./iosPush");
const apn = require("apn");
const gcm = require('node-gcm');
const FCM = require('./FCMPush');
const androidPush = require("./androidPush");
const config = require("../lib.js").getConfig();
const iosWorkerPushServerIsProd = parseInt(config.MY_IOS_WORKER_SERVICE_IS_PROD) === 1;
const iosClientPushServerIsProd = parseInt(config.MY_IOS_CLIENT_SERVICE_IS_PROD) === 1;
const serviceLog = require('../logger').serviceLogger;
const sqlManager = require('../../dbs/sqlManager');

/**
 * Push Service
 * @constructor
 */
function PushService() {
    //Service to send push data to ios worker app
    this.iosWorkerAppSenderService = null;

    //Object pool of services to send push data to android worker apps
    this.androidWorkerAppSenderService = {};

    //Object pool of services to send push data to ios client apps
    this.iosClientAppSenderService = {};

    //Object pool of services to send push data to android client apps
    this.androidClientAppSenderService = {};
}

/**
 * Sand push message to many devices
 * @param {String|Number} tenantId
 * @param {String} device
 * @param {String|Array} tokenArr
 * @param {String} typeApp
 * @param {Number} clientAppId
 * @param {Object} settings
 */
PushService.prototype.sendPush = function (tenantId, device, tokenArr, typeApp, clientAppId, settings) {
    const self = this;
    serviceLog('info', "Called service method: sendPush");
    if (typeApp === null || typeof typeApp === 'undefined') {
        typeApp = "worker";
    }

    return new Promise((resolve, reject) => {
        if (device === 'ios') {
            self.getIosPushService(typeApp, tenantId, clientAppId, (err, service) => {
                if (err || !service) {
                    serviceLog('error', err.message);
                    return reject(err);
                } else {
                    const iosPushMessage = new iosPush(service, tokenArr, settings);
                    iosPushMessage.sendPush((err, result) => {
                        if (err) {
                            serviceLog('error', err.message);
                            return reject(err);
                        } else {
                            return resolve(result);
                        }
                    });
                }
            });
        }
        if (device === 'android') {
            self.getAndroidPushService(typeApp, tenantId, clientAppId, (err, service) => {
                if (err || !service) {
                    serviceLog('error', err.message);
                    return reject(err);
                } else {

                    const androidPushMessage = new androidPush(service, tokenArr, settings);
                    androidPushMessage.sendPush((err, result) => {
                        if (err) {
                            serviceLog('error', err.message);
                            return reject(err);
                        } else {
                            return resolve(result);
                        }
                    });
                }
            });
        }
    });
};


/**
 * Destroy  IosWorkerAppSenderService, using when change  ios certs  for worker app in database
 */
PushService.prototype.destroyIosWorkerAppSenderService = function () {
    serviceLog('info', "Called service method: destroyIosWorkerAppSenderService");
    const self = this;
    try {
        self.iosWorkerAppSenderService.service.shutdown();
    } catch (err) {
        serviceLog('error', err.message);
    }
    self.iosWorkerAppSenderService = null;
    return true;
};

/**
 * Destroy  androidWorkerAppSenderService, using when change  gcm key  for worker app in database
 */
PushService.prototype.destroyAndroidWorkerAppSenderService = function (tenantId) {
    serviceLog('info', "Called service method: androidWorkerAppSenderService");
    const self = this;
    self.androidWorkerAppSenderService[tenantId] = null;
    return true;
};

/**
 * Destroy  iosClientAppSenderService, using when change  ios certs   for client app in database
 * @param  clientAppId
 */
PushService.prototype.destroyIosClientAppSenderService = function (clientAppId) {
    serviceLog('info', "Called service method: destroyIosClientAppSenderService");
    const self = this;
    try {
        self.iosClientAppSenderService[clientAppId].service.shutdown();
    } catch (err) {
        serviceLog('error', err.message);
    }
    self.iosClientAppSenderService[clientAppId] = null;
    return true;
};

/**
 * Destroy  androidClientAppSenderService, using when change  gcm key   for client app in database
 * @param  clientAppId
 */
PushService.prototype.destroyAndroidClientAppSenderService = function (clientAppId) {
    serviceLog('info', "Called service method: destroyAndroidClientAppSenderService");
    const self = this;
    self.androidClientAppSenderService[clientAppId] = null;
    return true;
};

/**
 * Get android push service or create it if not exist
 * For workerApp service in iosWorkerAppSenderService
 * For clientApp service in object pool androidWorkerAppSenderService
 * @param  typeApp
 * @param  tenantId
 * @param  clientAppId
 * @param {Function} callback
 */
PushService.prototype.getAndroidPushService = function (typeApp, tenantId, clientAppId, callback) {
    serviceLog('info ', "Called service method: getAndroidPushService");
    const self = this;
    if (typeApp === 'worker') {
        if (self.androidWorkerAppSenderService[tenantId] !== null && typeof self.androidWorkerAppSenderService[tenantId] === "object"
            && typeof self.androidWorkerAppSenderService[tenantId] !== "undefined") {
            serviceLog('info', `Service for sending push to android worker app is already exist. Return It.`);
            callback(null, self.androidWorkerAppSenderService[tenantId]);
        } else {
            sqlManager.getAndroidPushApiKey(typeApp, tenantId, clientAppId, (err, gsmKey) => {
                if (err) {
                    return callback(err);
                } else {
                    const service = new FCM(gsmKey);
                    self.androidWorkerAppSenderService[tenantId] = service;
                    callback(null, service);
                }
            });
        }
    } else if (typeApp === 'client') {
        //check object pool
        if (self.androidClientAppSenderService[clientAppId] !== null && typeof self.androidClientAppSenderService[clientAppId] === "object"
            && typeof self.androidClientAppSenderService[clientAppId] !== "undefined") {
            serviceLog('info', "Service for sending push to android client app is already exist.Return It.");

            callback(null, self.androidClientAppSenderService[clientAppId]);
        } else {
            sqlManager.getAndroidPushApiKey(typeApp, tenantId, clientAppId, (err, gsmKey) => {
                if (err) {
                    return callback(err);
                } else {
                    const service = new FCM(gsmKey);
                    //save service at object pool
                    self.androidClientAppSenderService[clientAppId] = service;
                    serviceLog('info', `Add new androidClientAppSenderService for clientAppId:${clientAppId}. Service:${JSON.stringify(service, null, 4)}`);
                    callback(null, service);
                }
            });
        }
    }
};
/**
 * Get ios push service  or create it if not exist
 * For workerApp service in iosWorkerAppSenderService
 * For clientApp service in object pool iosClientAppSenderService
 * @param  typeApp
 * @param  tenantId
 * @param  clientAppId
 * @param  {Function} callback
 */
PushService.prototype.getIosPushService = function (typeApp, tenantId, clientAppId, callback) {
    serviceLog('info', "Called service method: getIosPushService");
    const self = this;
    serviceLog('info', `typeApp:${typeApp} tenantId:${tenantId} clientAppId:${clientAppId}`);
    if (typeApp === 'worker') {
        if (self.iosWorkerAppSenderService !== null) {
            serviceLog('info', "Service for sending push to ios worker app is already exist. Return it.");
            console.log("IOS CLIENT PUSH SERVICE");
            callback(null, self.iosWorkerAppSenderService);
        } else {
            sqlManager.getIosPushApiKey(typeApp, tenantId, clientAppId, (err, iosCert) => {
                if (err) {
                    return callback(err);
                } else {
                    self.createApnConnection(iosCert, iosWorkerPushServerIsProd, (err, service) => {
                        if (err) {
                            return callback(err);
                        }
                        callback(null, service);
                    });
                }
            });
        }
    }
    if (typeApp === 'client') {
        //check object pool
        if (self.iosClientAppSenderService[clientAppId] !== null && typeof self.iosClientAppSenderService[clientAppId] === "object"
            && typeof self.iosClientAppSenderService[clientAppId] !== "undefined") {
            serviceLog('info', "Service for sending push to ios client app is already exist.Return it.");
            callback(null, self.iosClientAppSenderService[clientAppId]);
        } else {
            sqlManager.getIosPushApiKey(typeApp, tenantId, clientAppId, (err, iosCert) => {
                if (err) {
                    serviceLog('error', err.message);
                    return callback(err);
                } else {
                    self.createApnConnection(iosCert, iosClientPushServerIsProd, (err, service) => {
                        if (err) {
                            serviceLog('error', err.message);
                            return callback(err);
                        }
                        serviceLog('info', service);
                        self.iosClientAppSenderService[clientAppId] = service;
                        serviceLog('info', `Add new iosClientAppSenderService for clientAppId:${clientAppId}. Service:${JSON.stringify(service, null, 4)}`);
                        callback(null, service);
                    });
                }
            });
        }
    }
};

/**
 * Create apn connection
 * @param  {string}   iosCert
 * @param  {boolean}  isProd
 * @param  {function} callback
 * @returns {Object}  apn.Connection
 */
PushService.prototype.createApnConnection = function (iosCert, isProd, callback) {

    let certString = iosCert.substring(iosCert.indexOf("Bag Attributes"), iosCert.indexOf("-----END CERTIFICATE-----"));
    certString = certString + '-----END CERTIFICATE-----';

   /* let keyString_2 = iosCert.substring(iosCert.lastIndexOf("Bag Attributes"), iosCert.lastIndexOf("-----END RSA PRIVATE KEY-----"));
    keyString_2 = keyString_2 + "-----END RSA PRIVATE KEY-----";*/
   // console.log(certString_2);

    let keyString  = iosCert.replace(certString, '');
    let topic = 'com.kabbi.client';

 /*   console.log(keyString);
    process.exit();*/



  /*  serviceLog('info', "Called service method: createApnConnection");
    serviceLog('info', "Raw iosCert:");
    serviceLog('info', iosCert);
    iosCert = iosCert.replace(/\+/gi, "%20");
    iosCert = decodeURIComponent(iosCert);
    serviceLog('info', "Clean iosCert:");
    serviceLog('info', iosCert);
    let certString = iosCert.substring(iosCert.indexOf("Bag Attributes"), iosCert.lastIndexOf("-----END CERTIFICATE-----"));
    certString = certString + '-----END CERTIFICATE-----';
    let topic = ''; //App topic, for example: com.gootax.client
    let a = "IOS Push Services: ";
    let b = "\n";
    let regExToFindTopic = new RegExp("(?:" + a + ")(.*?)(?:" + b + ")", "ig"); //set ig flag for global search and case insensitive
    let topicRes = regExToFindTopic.exec(certString);
    if (topicRes && topicRes.length >= 1) //RegEx has found something and has more than one entry.
    {
        topic = topicRes[1];
    } else {
        b = "\r\n";
        regExToFindTopic = new RegExp("(?:" + a + ")(.*?)(?:" + b + ")", "ig"); //set ig flag for global search and case insensitive
        topicRes = regExToFindTopic.exec(certString);
        if (topicRes && topicRes.length >= 1) {
            topic = topicRes[1];
        }
    }
    serviceLog('info', `certString: ${certString}`);


    certString = new Buffer(certString);
    let keyString = iosCert.substring(iosCert.lastIndexOf("Bag Attributes"), iosCert.lastIndexOf("-----END RSA PRIVATE KEY-----"));
    keyString = keyString + "-----END RSA PRIVATE KEY-----";



    serviceLog('info', `keyString: ${keyString}`);
    serviceLog('info', `isProd: ${isProd}`);
    serviceLog('info', `topic: ${topic}`);*/

    certString = new Buffer(certString);
    keyString = new Buffer(keyString);
    const options = {
        cert: certString,
        key: keyString,
        production: isProd,
        fastMode: true,
        passphrase: 'kabbiclientpushcert2#'
    };


    try {
        const service = new apn.Provider(options);
        service.on("connected", () => {
            serviceLog('info', "Apns service: Connected");
        });

        service.on("timeout", () => {
            serviceLog('info', "Apns service: Connection Timeout");
        });

        service.on("disconnected", () => {
            serviceLog('info', "Apns service: Disconnected from APNS");
        });
        service.on("socketError", console.error);
        const conn = {
            topic: topic,
            service: service
        };


        serviceLog('info', `daimond + create -  object`);

        return callback(null, conn);
    } catch (err) {

        console.log('error');
        serviceLog("error", err.message);
        return callback(err);
    }


};

module.exports = exports = new PushService();
