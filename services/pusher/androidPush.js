'use strict';

//const gcm = require('node-gcm');
//const FCM = require('./FCMPush');
const serviceLog = require('../logger').serviceLogger;
const async = require('async');

/**
 * Creates a Push of ANDROID device
 * @constructor
 * @param {Object} senderService
 * @param {Array|String} tokenArr
 * @param {Object} settings
 */
function AndroidPush(senderService, tokenArr, settings) {
    serviceLog('info', "Called service method: androidPush");
    if (typeof tokenArr === "string") {
        tokenArr = [tokenArr];
    }
    this.tokenArr = tokenArr;
    this.title = settings.title;
    this.message = settings.message;
    this.contentAvailable = settings.contentAvailable;
    this.actionLocKey = settings.actionLocKey;
    this.category = settings.category;
    this.sound = settings.sound;
    this.icon = settings.icon;
    this.command = settings.command;
    this.command_params = settings.command_params;
    this.gsmSender = senderService;
    serviceLog('info', `Android push settings: ${JSON.stringify(settings)}`);
    serviceLog('info', `Android push tokenArr: ${JSON.stringify(tokenArr)}`);

}


/**
 * Send Push to one android device
 * @returns {Boolean}
 */
AndroidPush.prototype.sendPush = function (cb) {
    const self = this;
    self.basePushSender((err, res) => {
        if (err) {
            cb(err);
        } else {
            cb(null, res);
        }
    });
};


AndroidPush.prototype.basePushSender = function (cb) {
   // формирование сообщения
  //  this.gsmSender =  new FCM(key);

    const self = this;
    const maxConsumers = 999;
    let date  = {};
    date.message  = (typeof self.message !== 'undefined') ? self.message : '' ;
    date.tickerText  = (typeof self.title !== 'undefined') ? self.title : '' ;
    date.sound  = (typeof self.sound !== 'undefined') ? self.sound : '' ;
    date.icon  = (typeof self.icon !== 'undefined') ? self.icon : '' ;
    date.command  = (typeof self.command !== 'undefined') ? self.command : '' ;
    date.command_params  = (typeof self.command_params !== 'undefined') ? self.command_params : {} ;

    let notification  = {};
    notification.title  = (typeof self.tickerText !== 'undefined') ? self.tickerText : '' ;
    notification.body  = (typeof self.message !== 'undefined') ? self.message : '' ;
    let tokenChunk =  chunkArray_fcm(self.tokenArr, maxConsumers);
    let message = {
        registration_ids: tokenChunk,
        notification: notification,
        data: date,
        time_to_live: 1800,
        priority: 'high',
    };

    /*console.log(message);
    console.log('Новове соообещние ' + '\n');
    const message_old = new gcm.Message({
        priority: 'high',
        timeToLive: 1800
    });
    message_old.addData('message', self.message);
    message_old.addData('tickerText', self.title);
    message_old.addData('sound', self.sound);
    message_old.addData('icon', self.icon);
    message_old.addData('command', self.command);
    message_old.addData('command_params', self.command_params);
    console.log(message_old);
    console.log('Старое соообещние ' + '\n');*/

    let deliveryResults = [];
    let tokenC= chunk(tokenChunk );   // self.tokenArr
    async.eachSeries(tokenC, function iterator(tokenChunk, callback) {
        self.gsmSender.send(message, function(err, result){
           if (err) {
                serviceLog('error', `GCM error:${err.message}`);
            } else {

               result.results = result.results.map((resObj) => {
                   resObj.device = tokenChunk.shift();
                   return resObj;
               });
               deliveryResults.push(result);
            }
            callback();
        });
    }, function done() {
        cb(null, {android_sender_report: deliveryResults});
    });



    /*const maxConsumers = 999;
    const self = this;
    const message = new gcm.Message({
        priority: 'high',
        timeToLive: 1800
    });
    message.addData('message', self.message);
    message.addData('tickerText', self.title);
    message.addData('sound', self.sound);
    message.addData('icon', self.icon);
    message.addData('command', self.command);
    message.addData('command_params', self.command_params);

    const tokenChunk = chunkArray(self.tokenArr, maxConsumers);

    let deliveryResults = [];
    async.eachSeries(tokenChunk, function iterator(tokenChunk, callback) {
        self.gsmSender.send(message, tokenChunk, (err, result) => {
            console.log(err);
            if (err) {
               // console.log(err);
                serviceLog('error', `GCM error:${err.message}`);
            } else {

                result.results = result.results.map((resObj) => {
                    resObj.device = tokenChunk.shift();
                    console.log(resObj);
                    return resObj;
                });

                deliveryResults.push(result);
            }
            callback();
        });
    }, function done() {
        cb(null, {android_sender_report: deliveryResults});
    });*/

};

function chunkArray(arr, chunk) {
    let i, j, tmp = [];
    for (i = 0, j = arr.length; i < j; i += chunk) {
        tmp.push(arr.slice(i, i + chunk));
    }
    return tmp;
}

function chunk(arr) {
    let tmp = [];
    tmp[0] = arr;

    return tmp;
}


function chunkArray_fcm(arr, chunk) {
    if ( arr.length < chunk){
        return arr;
    }
    return arr.slice(0, chunk);
}


module.exports = AndroidPush;