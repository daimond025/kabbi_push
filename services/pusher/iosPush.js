'use strict';

const apn = require("apn");
const serviceLog = require('../logger').serviceLogger;


/**
 * Creates a Push of IOS deivece
 * @constructor
 * @param {Object} senderService
 * @param {Array|String} tokenArr
 * @param {Object} settings
 */
function IosPush(senderService, tokenArr, settings) {
    const self = this;
    serviceLog('info', "Called IOS service method: iosPush");
    if (typeof tokenArr === "string") {
        tokenArr = [tokenArr];
    }
    self.tokenArr = tokenArr;
    self.title = settings.title;
    self.message = settings.message;
    self.contentAvailable = settings.contentAvailable;
    self.actionLocKey = settings.actionLocKey;
    self.category = settings.category;
    self.sound = settings.sound;
    self.icon = settings.icon;
    self.command = settings.command;
    self.command_params = settings.command_params;
    self.apnSender = senderService.service;
    self.topic = senderService.topic;
    serviceLog('info', `IOS push settings: ${JSON.stringify(settings)}`);
    serviceLog('info', `IOS push tokenArr: ${JSON.stringify(tokenArr)}`);
}

/**
 * Send Push to one ios device
 */
IosPush.prototype.sendPush = function (cb) {
    const self = this;
    self.basePushSender((err, res) => {
        if (err) {
            cb(err);
        } else {
            cb(null, res);
        }
    });
};


IosPush.prototype.basePushSender = function (cb) {
    const self = this;
    const title = self.title;
    const message = self.message;
    const actionLocKey = self.actionLocKey;
    const sound = self.sound;
    const category = self.category;
    const contentAvailable = self.contentAvailable;
    const command_params = self.command_params;
    const command = self.command;
    const apnSender = self.apnSender;
    const topic = self.topic;
    const note = new apn.Notification();
    note.topic = topic;
    note.alert = {
        'title': title,
        'body': message,
        'action-loc-key': actionLocKey
    };
    note.badge = 0;
    note.sound = sound;
    note.category = category;
    note.contentAvailable = contentAvailable;
    //Custom property
    note.payload.aps = {
        "command_params": command_params,
        "command": command
    };
    note.payload.command_params = command_params;
    note.payload.command = command;
    note.expiry = Math.floor(Date.now() / 1000) + 1800;
    apnSender.send(note, self.tokenArr).then((result) => {
        cb(null, {ios_sender_report: [result]});
    });
};


module.exports = IosPush;