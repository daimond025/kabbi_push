'use strict';
const syslog = require('modern-syslog');
const config = require('./lib').getConfig();
const serviceVersion = require('./lib').getServiceVersion();


/**
 * Service Logger
 * @param  {String} category
 * @param  {String} message
 */
function serviceLogger(category, message) {
    const prefix = '[main] ';
    if (typeof message === "object" && message !== null) {
        message = JSON.stringify(message);
    }
    message = prefix + message;
    syslog.init(config.SYSLOG_PUSH_SERVICE_IDENT + '-' + serviceVersion, null, config.SYSLOG_PUSH_SERVICE_FACILITY);
    switch (category) {
        case 'info':
            syslog.log(syslog.level.LOG_INFO, message);
            break;
        case 'error':
            syslog.log(syslog.level.LOG_ERR, message);
            break;
        default:
            syslog.log(syslog.level.LOG_INFO, message);
    }
}

/**
 * Order logger
 * @param  {Number} orderId
 * @param  {String} category
 * @param  {String} message
 */
function orderLogger(orderId, category, message) {
    const prefix = '[order] order_id=' + orderId + ' ';
    if (typeof message === "object" && message !== null) {
        message = JSON.stringify(message);
    }
    message = prefix + message;
    syslog.init(config.SYSLOG_ORDER_IDENT + '-' + serviceVersion, null, config.SYSLOG_ORDER_FACILITY);
    switch (category) {
        case 'info':
            syslog.log(syslog.level.LOG_INFO, message);
            break;
        case 'error':
            syslog.log(syslog.level.LOG_ERR, message);
            break;
        default:
            syslog.log(syslog.level.LOG_INFO, message);
    }
}

/**
 * Bulk notify logger
 * @param  {Number} notificationId
 * @param  {String} category
 * @param  {String} message
 */
function bulkNotifyLogger(notificationId, category, message) {
    const prefix = '[notification] notification_id=' + notificationId + ' ';
    if (typeof message === "object" && message !== null) {
        message = JSON.stringify(message);
    }
    message = prefix + message;
    syslog.init(config.SYSLOG_NOTIFICATION_IDENT + '-' + serviceVersion, null, config.SYSLOG_NOTIFICATION_FACILITY);
    switch (category) {
        case 'info':
            syslog.log(syslog.level.LOG_INFO, message);
            break;
        case 'error':
            syslog.log(syslog.level.LOG_ERR, message);
            break;
        default:
            syslog.log(syslog.level.LOG_INFO, message);
    }
}

module.exports = {
    serviceLogger,
    orderLogger,
    bulkNotifyLogger
};

