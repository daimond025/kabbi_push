'use strict';

const packageInfo = require('../package.json');
const mongoCheckInsert = require('../dbs/mongoManager').checkInsert;
const mongoCheckUpdate = require('../dbs/mongoManager').checkUpdate;
const mongoCheckSelect = require('../dbs/mongoManager').checkSelect;
const mongoCheckDelete = require('../dbs/mongoManager').checkDelete;
const request = require('request');
const config = require("./lib").getConfig();
const geoservice_Api = config.SERVICE_GEO_URL;

const sqlManager = require('../dbs/sqlManager');
const redisCheckManager = require('../dbs/redisCheckManager');
const uuid = require('uuid/v4');

function version(req, res, next) {
    const name = packageInfo.name;
    const version = packageInfo.version;
    res.setHeader('Content-Type', 'text/plain');
    let info = `${name} v${version}
work on nodejs ${process.version}`;
    res.send(info);
}

function status(req, res, next) {
    const STATUS_OK = 'OK   ';
    const STATUS_ERROR = 'ERROR';
    const name = packageInfo.name;
    const version = packageInfo.version;
    let isNot = ' ';
    let hasError = false;
    let redisStatus, redisError,
        dbStatus, dbError,
        mongoStatus, mongoError,
        geoServiceStatus, geoServiceError;
    redisChecker()
        .then(() => {
            redisStatus = STATUS_OK;
            redisError = '';
        })
        .catch(redisErr => {
            redisStatus = STATUS_ERROR;
            redisError = `(${redisErr.message})`;
            hasError = true;
        })
        .then(() => {
            return dbChecker();
        })
        .then(() => {
            dbStatus = STATUS_OK;
            dbError = '';
        })
        .catch(sqlErr => {
            dbStatus = STATUS_ERROR;
            dbError = `(${sqlErr.message})`;
            hasError = true;
        })
        .then(() => {
            return mongoChecker();
        })
        .then(() => {
            mongoStatus = STATUS_OK;
            mongoError = '';
        })
        .catch(mongoErr => {
            mongoStatus = STATUS_ERROR;
            mongoError = `(${mongoErr.message})`;
            hasError = true;
        })
        .then(() => {
            return geoServiceChecker()
        })
        .then(() => {
            geoServiceStatus = STATUS_OK;
            geoServiceError = '';
        })
        .catch(geoErr => {
            geoServiceStatus = STATUS_ERROR;
            geoServiceError = `(${geoErr.message})`;
            hasError = true;
        })
        .then(() => {
            if (hasError) {
                isNot = ' NOT ';
            }
            let status = `${name} v${version}
work on nodejs ${process.version}
            
[ ${dbStatus} ] database "db" ${dbError}
[ ${mongoStatus} ] database "mongodb" ${mongoError}
[ ${redisStatus} ] database "redis" ${redisError}
[ ${geoServiceStatus} ] service "geo" ${geoServiceError}

SERVICE${isNot}OPERATIONAL`;
            res.setHeader('Content-Type', 'text/plain');
            res.send(`${status}`);
        });
}

/**
 * Check mongo
 * @return {Promise}
 */
function mongoChecker() {
    return new Promise((resolve, reject) => {
        let key = uuid();
        mongoCheckInsert(key, 'push_test_set')
            .then(() => {
                return mongoCheckSelect(key)
            })
            .then(() => {
                return mongoCheckUpdate(key, 'push_test_update')
            })
            .then(() => {
                return mongoCheckDelete(key)
            })
            .then(() => {
                resolve('OK');
            })
            .catch(err => {
                reject(err);
            })
    })
}

/**
 * Check redis
 * @return {Promise}
 */
function redisChecker() {
    return new Promise((resolve, reject) => {
        let key = uuid();
        redisCheckManager.setValue(key, 'push_test_set')
            .then(() => {
                return redisCheckManager.getValue(key)
            })
            .then(() => {
                return redisCheckManager.updateValue(key, 'push_test_update')
            })
            .then(() => {
                return redisCheckManager.deleteKey(key)
            })
            .then(() => {
                resolve('OK');
            })
            .catch(err => {
                reject(err);
            })
    })
}

/**
 * Check db
 * @return {Promise}
 */
function dbChecker() {
    return new Promise((resolve, reject) => {
        let key = uuid();
        sqlManager.checkInsert(key, 'push_test_insert')
            .then(() => {
                return sqlManager.checkSelect(key)
            })
            .then(() => {
                return sqlManager.checkUpdate(key, 'push_test_update')
            })
            .then(() => {
                return sqlManager.checkDelete(key)
            })
            .then(() => {
                resolve('OK');
            })
            .catch(err => {
                reject(err);
            })
    })
}

function geoServiceChecker() {
    return new Promise((resolve, reject) => {
        const requestOptions = {
            url: `${geoservice_Api}version`
        };

        function sendRequest(error, response, body) {
            if (!error && parseInt(response.statusCode) === 200) {
                resolve();
            } else {
                if (error) {
                    reject(error);
                } else {
                    reject(new Error(`bad status code: ${response.statusCode}`))
                }
            }
        }

        request(requestOptions, sendRequest);
    })
}


module.exports = {
    version, status
};
