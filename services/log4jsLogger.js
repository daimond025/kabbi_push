'use strict';

const log4js = require('log4js');
log4js.configure({
    appenders: [
        {type: 'console'},
        {
            type: 'file',
            filename: 'logs/push_service.log',
            category: 'push_service',
            maxLogSize: 52428800,
            backups: 5

        },
    ]
});

const serviceLog = log4js.getLogger('push_service');
/**
 * service logger
 * @param category
 * @param message
 */
function serviceLogger(category, message) {
    switch (category) {
        case 'info':
            serviceLog.info(message);
            break;
        case 'error':
            serviceLog.error(message);
            break;
        default:
            serviceLog.info(message);
    }
}


exports.serviceLogger = serviceLogger;
