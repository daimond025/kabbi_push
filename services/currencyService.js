'use strict';
const myCache = require('../dbs/cache');
const sqlManager = require('../dbs/sqlManager');

/**
 * Get currency code
 * @param currencyId
 * @return {Promise<any>}
 */
function getCode(currencyId) {
    return new Promise((resolve, reject) => {
        const cacheKey = `currency_${currencyId}`;
        myCache.get(cacheKey, (err, currencyCode) => {
            if (err) {
                return reject(err);
            }
            if (currencyCode) {
                return resolve(currencyCode)
            }
            sqlManager.getCurrencyCodeById(currencyId, (err, currencyCode) => {
                if (err) {
                    return reject(err);
                }
                myCache.set(cacheKey, currencyCode, (err, result) => {
                    if (err) {
                        return reject(err);
                    }
                    return resolve(currencyCode);
                });
            });
        });

    });
}

exports.getCode = getCode;