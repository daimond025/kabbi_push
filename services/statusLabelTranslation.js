'use strict';
const positionInfo = require('./workerPositionManager').positionInfo;
const statusInfo = require('./orderStatusManager').statusInfo;


/**
 * Get translated status label for position
 * @param req
 * @param statusGroup
 * @param statusId
 * @param positionId
 * @return {*}
 */
module.exports = function (req, statusGroup, statusId, positionId) {
    let statusLabel;
    switch (statusGroup) {
        case "new":
            // Идет поиск исполнителя
            switch (positionId) {
                case positionInfo.simple_taxi_driver.position_id:
                    statusLabel = req.i18n.__('Search cars...');
                    break;
                case positionInfo.simple_truck_driver.position_id:
                    statusLabel = req.i18n.__('Search cars...');
                    break;
                case positionInfo.simple_courier.position_id:
                case positionInfo.courier_on_auto.position_id:
                case positionInfo.courier_on_moto.position_id:
                    statusLabel = req.i18n.__('Search courier...');
                    break;
                case positionInfo.simple_master.position_id:
                    statusLabel = req.i18n.__('Search master...');//
                    break;
                default:
                    statusLabel = req.i18n.__('Search cars...');
                    break;
            }
            break;
        case "pre_order":
            // Новый пред заказ
            if (statusId === statusInfo.new_pre_order.status_id || statusId === statusInfo.new_pre_order_no_parking.status_id) {
                statusLabel = req.i18n.__('Order is accepted');
                break;
                //Отказ исполнителя
            } else if (statusId === statusInfo.worker_refused_preorder.status_id) {
                switch (positionId) {
                    case positionInfo.simple_taxi_driver.position_id:
                        statusLabel = req.i18n.__('Drivers order refusing');
                        break;
                    case positionInfo.simple_truck_driver.position_id:
                        statusLabel = req.i18n.__('Drivers order refusing');
                        break;
                    case positionInfo.simple_courier.position_id:
                    case positionInfo.courier_on_auto.position_id:
                    case positionInfo.courier_on_moto.position_id:
                        statusLabel = req.i18n.__('Courier order refusing');
                        break;
                    case positionInfo.simple_master.position_id:
                        statusLabel = req.i18n.__('Master order refusing');//
                        break;
                    default:
                        statusLabel = req.i18n.__('Drivers order refusing');
                        break;
                }
                break;
                //Исполнитель принял заказ
            } else if (statusId === statusInfo.worker_accepted_preorder.status_id) {
                switch (positionId) {
                    case positionInfo.simple_taxi_driver.position_id:
                        statusLabel = req.i18n.__('Driver has accepted an order');
                        break;
                    case positionInfo.simple_truck_driver.position_id:
                        statusLabel = req.i18n.__('Driver has accepted an order');
                        break;
                    case positionInfo.simple_courier.position_id:
                    case positionInfo.courier_on_auto.position_id:
                    case positionInfo.courier_on_moto.position_id:
                        statusLabel = req.i18n.__('Courier has accepted an order');
                        break;
                    case positionInfo.simple_master.position_id:
                        statusLabel = req.i18n.__('Master has accepted an order');//
                        break;
                    default:
                        statusLabel = req.i18n.__('Driver has accepted an order');
                        break;
                }
                break;
            }
            break;
        case "car_assigned":
            switch (positionId) {
                case positionInfo.simple_taxi_driver.position_id:
                    statusLabel = req.i18n.__('The driver left');
                    break;
                case positionInfo.simple_truck_driver.position_id:
                    statusLabel = req.i18n.__('The driver left');
                    break;
                case positionInfo.simple_courier.position_id:
                case positionInfo.courier_on_auto.position_id:
                case positionInfo.courier_on_moto.position_id:
                    statusLabel = req.i18n.__('The courier left');
                    break;
                case positionInfo.simple_master.position_id:
                    statusLabel = req.i18n.__('The master left');//
                    break;
                default:
                    statusLabel = req.i18n.__('The driver left');
                    break;
            }
            break;
        case "car_at_place":
            switch (positionId) {
                case positionInfo.simple_taxi_driver.position_id:
                    statusLabel = req.i18n.__('The driver arrived');
                    break;
                case positionInfo.simple_truck_driver.position_id:
                    statusLabel = req.i18n.__('The driver arrived');
                    break;
                case positionInfo.simple_courier.position_id:
                case positionInfo.courier_on_auto.position_id:
                case positionInfo.courier_on_moto.position_id:
                    statusLabel = req.i18n.__('The courier arrived');
                    break;
                case positionInfo.simple_master.position_id:
                    statusLabel = req.i18n.__('The master arrived');//
                    break;
                default:
                    statusLabel = req.i18n.__('The driver arrived');
                    break;
            }
            break;
        case "executing":
            statusLabel = req.i18n.__('Execution of an order');
            break;
        case "completed":
            statusLabel = req.i18n.__('Completed');
            break;
        case "rejected":
            statusLabel = req.i18n.__('Rejected');
            break;
    }
    return statusLabel;
};