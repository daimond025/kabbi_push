'use strict';

const http = require("http");
const express = require('express');
const expressValidator = require('express-validator');
const bodyParser = require('body-parser');
const nodemailer = require('nodemailer');
const cluster = require('cluster');
const os = require('os');
const serviceLogger = require('./services/logger').serviceLogger;
const i18n = require('i18n-2');
const config = require("./services/lib.js").getConfig();
const pusher = require('./services/pusher/pushService');
const mainRouter = require('./api/routes/main');
const numCPUs = os.cpus().length;

if (cluster.isMaster) {
    // Master:
    // fork workers by number of CPU cores
    for (let i = 0; i < numCPUs; ++i) {
        const childWorker = cluster.fork();
        const childWorkerPID = childWorker.process.pid;
        serviceLogger('info', `worker with pid: ${childWorkerPID} started`);
    }

    cluster.on('exit', function (deadWorker, code, signal) {
        const oldPID = deadWorker.process.pid;
        serviceLogger('error', `worker with pid: ${oldPID} started`);
        serviceLogger('error', `worker die code: ${code} signal: ${signal}`);
        // Restart the worker
        const worker = cluster.fork();
        // Note the process IDs
        const newPID = worker.process.pid;
        serviceLogger('info', `worker with pid: ${newPID} born`);
    });
    cluster.on('message', (worker, msg, handle) => {
        serviceLogger('info', `new cluster message! sender: ${worker.process.pid}; message: ${JSON.stringify(msg, null, 4)}`);
        let workerSenderId = worker.id;
        for (let id in cluster.workers) {
            if (cluster.workers[id].id !== workerSenderId) {
                cluster.workers[id].send(msg);
            }
        }
    })
} else {
    const app = express();
    i18n.expressBind(app, {
        // setup some locales - other locales default to en silently
        locales: [
            'ru', 'en', 'sr', 'az', 'fa', 'fi', 'tk', 'sr-ME', 'uz', 'hy', 'ar', 'de'
        ],
        // you may alter a site wide default locale
        defaultLocale: 'de',
        // where to store json files - defaults to './locales' relative to modules directory
        directory: './locales',
        // whether to write new locale information to disk - defaults to true
        updateFiles: false,
        autoReload: false,
        // what to use as the indentation unit - defaults to "\t"
        indent: "\t",
        // setting extension of json files - defaults to '.json' (you might want to set this to '.js' according to webtranslateit)
        extension: '.json',
        // enable object notation
        objectNotation: false,
        // setting of log level DEBUG - default to require('debug')('i18n:debug')
        logDebugFn: err => {
            serviceLogger('info', err.message);
        },
        // setting of log level WARN - default to require('debug')('i18n:warn')
        logWarnFn: err => {
            serviceLogger('error', err.message);
        },
        // setting of log level ERROR - default to require('debug')('i18n:error')
        logErrorFn: err => {
            serviceLogger('error', err.message);
        }
    });
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(expressValidator({
        errorFormatter: (param, msg, value) => {
            return {
                result: 0,
                error: `${msg}. Param:${param}. Value:${value}`
            };
        }
    }));
    app.use('/', mainRouter);


    const server = http.createServer(app);
    server.listen(config.MY_BIND_PORT, config.MY_BIND_HOST, () => {
        serviceLogger('info', 'Server started at ' + config.MY_BIND_HOST + ':' + config.MY_BIND_PORT);
    });

    process.on('message', msg => {
        serviceLogger('info', `new process message! message: ${JSON.stringify(msg)}`);
        if (msg.cmd === 'destroy_ios_client_app_sender_service') {
            pusher.destroyIosClientAppSenderService(msg.app_id);
        }
        if (msg.cmd === 'destroy_android_client_app_sender_service') {
            pusher.destroyAndroidClientAppSenderService(msg.app_id);
        }
        if (msg.cmd === 'destroy_android_driver_app_sender_service') {
            pusher.destroyAndroidWorkerAppSenderService(msg.tenant_id);
        }
        if (msg.cmd === 'destroy_ios_driver_app_sender_service') {
            pusher.destroyIosWorkerAppSenderService();
        }
    });

    process.on('uncaughtException', (err) => {
        serviceLogger('error', 'Critical error: ' + err.message);
        serviceLogger('error', 'Error stack: ' + err.stack);
        const transporter = nodemailer.createTransport({
            service: config.MAIL_SUPPORT_PROVIDER,
            auth: {
                user: config.MAIL_SUPPORT_LOGIN,
                pass: config.MAIL_SUPPORT_PASSWORD
            }
        });
        const mailOptions = {
            from: config.MAIL_SUPPORT_LOGIN,
            to: config.MAIL_SUPPORT_LOGIN,
            subject: config.MAIL_SUPPORT_SUBJECT_ERROR,
            text: err.message,
            html: err.message + "  |  " + err.stack
        };
        transporter.sendMail(mailOptions, (err, info) => {
            if (err) {
                serviceLogger('error', err.message);
            } else {
                serviceLogger('error', 'Message sent: ' + info.response);
            }
            try {
                const killTimer = setTimeout(() => {
                    process.exit(1);
                }, 3000);
                killTimer.unref();
                server.close();
                cluster.worker.disconnect();
            } catch (er2) {
                serviceLogger('error', er2.stack);
            }
        });

    });

    process.on('warning', (err) => {
        serviceLogger('error', 'Warning: ' + err.message);
        serviceLogger('error', 'Warning stack: ' + err.stack);
        const transporter = nodemailer.createTransport({
            service: config.MAIL_SUPPORT_PROVIDER,
            auth: {
                user: config.MAIL_SUPPORT_LOGIN,
                pass: config.MAIL_SUPPORT_PASSWORD
            }
        });
        const mailOptions = {
            from: config.MAIL_SUPPORT_LOGIN,
            to: config.MAIL_SUPPORT_LOGIN,
            subject: config.MAIL_SUPPORT_SUBJECT_WARNING,
            text: err.message,
            html: err.message + "  |  " + err.stack
        };
        transporter.sendMail(mailOptions, (err, info) => {
            if (err) {
                serviceLogger('error', err.message);
            } else {
                serviceLogger('error', 'Message sent: ' + info.response);
            }
        });
    });
}

