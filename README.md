## Common description

Сервис на node.js для рассылки push-уведомлений 
на клиентские и водительские приложения (ios/android)

## Requirements version of Node.js
Node version : ^v.6.9.1

## Install Dependencies

```bash
npm install
```
    
## How to run server
* Заполнить .env файл по примеру .env.example файла


* Проверить корректность .env файла: 
```bash
npm run check-env
``` 
При успешном выполнении в консоль выведет: OK. В случае ошибки в консоль выведет описание ошибки.

* Запустить сервер: 
```bash
npm run start
```

@TODO
* Переделать на запуск через NODE_ENV
* ЗАдать параметры памяти такие как –max-old-space-size  и тд.
## API description
@TODO