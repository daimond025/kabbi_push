'use strict';

const express = require('express');
const router = express.Router();
const logger = require('../../services/logger').serviceLogger;
const serviceInfo = require('../../services/serviceInfo');
// Worker services
const send_free_order_for_all = require('../../notificationActions/worker/send_free_order_for_all');
const send_pre_order_for_all = require('../../notificationActions/worker/send_pre_order_for_all');
const send_push_message_to_worker = require('../../notificationActions/worker/send_push_message_to_worker');
const send_push_message_to_all_workers = require('../../notificationActions/worker/send_push_message_to_all_workers');
const send_to_worker_reject_order = require('../../notificationActions/worker/send_to_worker_reject_order');
const reject_worker_from_order = require('../../notificationActions/worker/reject_worker_from_order');
const send_to_worker_new_order = require('../../notificationActions/worker/send_to_worker_new_order');
const send_to_worker_update_order = require('../../notificationActions/worker/send_to_worker_update_order');
const send_to_worker_remind_pre_order = require('../../notificationActions/worker/send_to_worker_remind_pre_order');
const worker_send_push_transaction = require('../../notificationActions/worker/worker_send_push_transaction');
const set_signal_sos = require('../../notificationActions/worker/set_signal_sos');
const unset_signal_sos = require('../../notificationActions/worker/unset_signal_sos');
const destroy_ios_worker_app_sender_service = require('../../notificationActions/worker/destroy_ios_worker_app_sender_service');
const destroy_android_worker_app_sender_service = require('../../notificationActions/worker/destroy_android_worker_app_sender_service');
const send_assign_at_order = require('../../notificationActions/worker/send_assign_at_order');
const send_refuse_order_assignment = require('../../notificationActions/worker/send_refuse_order_assignment');
// Client services
const client_send_push_transaction = require('../../notificationActions/client/client_send_push_transaction');
const send_push_notification_for_client = require('../../notificationActions/client/send_push_notification_for_client');
const destroy_ios_client_app_sender_service = require('../../notificationActions/client/destroy_ios_client_app_sender_service');
const destroy_android_client_app_sender_service = require('../../notificationActions/client/destroy_android_client_app_sender_service');
//Common services
const bulk_notify = require('../../notificationActions/common/bulk_notify');

router.use(function (req, res, next) {
    logger('info', 'Called method: ' + req.method + ' ' + req.url);
    logger('info', 'Query params: ' + JSON.stringify(req.query));
    next();
});

router.get('/', function (req, res) {
    res.send('Hello! I am Gootax push-service');
});

//Worker reqs
router.get('/notification/send_free_order_for_all', send_free_order_for_all);
router.get('/notification/send_pre_order_for_all', send_pre_order_for_all);
router.get('/notification/send_to_worker_reject_order', send_to_worker_reject_order);
router.get('/notification/reject_worker_from_order', reject_worker_from_order);
router.get('/notification/send_to_worker_new_order', send_to_worker_new_order);
router.get('/notification/send_to_worker_update_order', send_to_worker_update_order);
router.get('/notification/send_to_worker_remind_pre_order', send_to_worker_remind_pre_order);
router.post('/notification/worker_send_push_transaction', worker_send_push_transaction);
router.get('/notification/set_signal_sos', set_signal_sos);
router.get('/notification/unset_signal_sos', unset_signal_sos);
router.post('/notification/send_push_message_to_all_workers', send_push_message_to_all_workers);
router.post('/notification/send_push_message_to_worker', send_push_message_to_worker);
router.get('/notification/destroy_ios_worker_app_sender_service', destroy_ios_worker_app_sender_service);
router.get('/notification/destroy_android_worker_app_sender_service', destroy_android_worker_app_sender_service);
router.get('/notification/send_assign_at_order', send_assign_at_order);
router.get('/notification/send_refuse_order_assignment', send_refuse_order_assignment);
//Client reqs
router.get('/notification/send_push_notification_for_client', send_push_notification_for_client);
router.post('/notification/client_send_push_transaction', client_send_push_transaction);
router.get('/notification/destroy_ios_client_app_sender_service', destroy_ios_client_app_sender_service);
router.get('/notification/destroy_android_client_app_sender_service', destroy_android_client_app_sender_service);
//Common reqs
router.post('/notification/bulk_notify', bulk_notify);
//For monitoring
router.get('/version', serviceInfo.version);
router.get('/status', serviceInfo.status);

module.exports = router;