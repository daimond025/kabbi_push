"use strict";

const config = require("../services/lib").getConfig();
const PHPUnserialize = require('php-unserialize');
const redis = require("redis");
const serviceLogger = require('../services/logger').serviceLogger;
const retryStrategy = require("./redisRetryStrategy");

/**
 * Manger for redis order db
 * @param  port
 * @param  ip
 */
function RedisOrderManager(port, ip) {
    this.DB_NUMBER = 1;
    //Orders
    this.redisClientDb = redis.createClient(port, ip, {
        retry_strategy: function (options) {
            return retryStrategy(options);
        }
    });

    this.redisClientDb.on("connect", () => {
        serviceLogger('info', `connected Redis db${this.DB_NUMBER}`);
    });
    this.redisClientDb.on("reconnecting", () => {
        serviceLogger('info', `reconnecting Redis db${this.DB_NUMBER}`);
    });

    this.redisClientDb.on("error", err => {
        serviceLogger('error', `Redis error in db${this.DB_NUMBER}: ${err.message}`);
    });

}


/**
 * Get order data by tenantId and orderId
 * @param  tenantId
 * @param  orderId
 * @param  {Function} callback
 */
RedisOrderManager.prototype.getOrder = function (tenantId, orderId, callback) {
    const self = this;
    self.redisClientDb.select(self.DB_NUMBER, function () {
        self.redisClientDb.hget([tenantId, orderId], (err, orderSerr) => {
            if (err) {
                return callback(err);
            }
            try {
                const order = PHPUnserialize.unserialize(orderSerr);
                if (order === null || typeof order === "undefined") {
                    return callback("No order at redisDb");
                }
                callback(null, order);
            } catch (err) {
                return callback(err);
            }

        });

    });
};


module.exports = exports = new RedisOrderManager(config.REDIS_MAIN_PORT, config.REDIS_MAIN_HOST);