"use strict";

const config = require("../services/lib").getConfig();
const PHPUnserialize = require('php-unserialize');
const redis = require("redis");
const async = require("async");
const serviceLogger = require('../services/logger').serviceLogger;
const retryStrategy = require("./redisRetryStrategy");

/**
 * Manager for redis workers db
 * @param  port
 * @param  ip
 * @returns {RedisWorkerManager}
 */
function RedisWorkerManager(port, ip) {
    this.DB_NUMBER = 0;
    //Workers
    this.redisClientDb0 = redis.createClient(port, ip, {retry_strategy: options => retryStrategy(options)});

    this.redisClientDb0.on("connect", () => {
        serviceLogger('info', `connected Redis db${this.DB_NUMBER}`);
    });
    this.redisClientDb0.on("reconnecting", () => {
        serviceLogger('info', `reconnecting Redis db${this.DB_NUMBER}`);
    });

    this.redisClientDb0.on("error", err => {
        serviceLogger('error', `Redis error in db${this.DB_NUMBER}`);
    });


}


/**
 * Get all active workers by tenantId
 * @param  tenantId
 * @param  {Function} callback
 */
RedisWorkerManager.prototype.getAllWorkers = function (tenantId, callback) {
    const self = this;
    self.redisClientDb0.select(self.DB_NUMBER, () => {
        self.redisClientDb0.hvals(tenantId, (err, workers) => {
            if (err) {
                return callback(err);
            }
            const workersResultArr = [];
            async.each(workers, (workerItem, callback) => {
                try {
                    const worker = PHPUnserialize.unserialize(workerItem);
                    if (worker !== null && typeof worker !== "undefined" && worker.worker && worker.geo && worker.position) {
                        workersResultArr.push(worker);
                    }
                    callback();
                } catch (err) {
                    return callback();
                }
            }, function (err) {
                if (err) {
                    return callback(err);
                }
                callback(null, workersResultArr);
            });

        });
    });
};

/**
 * Get worker data by tenantId and his callsign
 * @param  tenantId
 * @param  callsign
 * @param  {Function} callback
 */
RedisWorkerManager.prototype.getWorker = function (tenantId, callsign, callback) {
    const self = this;
    self.redisClientDb0.select(self.DB_NUMBER, () => {
        self.redisClientDb0.hget([tenantId, callsign], (err, workerItem) => {
            if (err) {
                return callback(err);
            }
            try {
                const worker = PHPUnserialize.unserialize(workerItem);
                if (worker === null || typeof worker === "undefined") {
                    return callback("No worker at redsDb");
                }
                if (!worker.worker || !worker.geo || !worker.position) {
                    return callback("Uncorrect worker data at redisDb");
                }
                callback(null, worker);
            } catch (err) {
                return callback(err);
            }
        });

    });
};

module.exports = exports = new RedisWorkerManager(config.REDIS_MAIN_PORT, config.REDIS_MAIN_HOST);