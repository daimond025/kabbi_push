"use strict";

const mysql = require('mysql');
const config = require("../services/lib").getConfig();
const iosWorkerPushServerIsProd = config.MY_IOS_WORKER_SERVICE_IS_PROD;
const serviceLogger = require('../services/logger').serviceLogger;

/**
 * SQL manager
 * @param db_Host
 * @param db_User
 * @param db_Password
 * @param db_Database
 * @constructor
 */
function SqlManager(db_Host, db_User, db_Password, db_Database) {
    this.pool = mysql.createPool({
        host: config.DB_MAIN_HOST,
        user: config.DB_MAIN_USERNAME,
        password: config.DB_MAIN_PASSWORD,
        database: config.DB_MAIN_NAME,
        connectionLimit: parseInt(config.DB_MAIN_CONNECTION_LIMIT),
        connectTimeout: parseInt(config.DB_MAIN_CONNECTION_TIMEOUT),
        acquireTimeout: parseInt(config.DB_MAIN_ACQUIRE_TIMEOUT),
    });
    this.pool.on('connection', connection => {
        serviceLogger('info', "Sql pool: connected!");
    });
    this.pool.on('enqueue', () => {
        serviceLogger('info', "Sql pool: waiting for available connection slot");
    });

    this.pool.on('error', (err) => {
        serviceLogger('error', `Sql pool: error: ${err.message}`);
    });

}

/**
 * Get tenant id by domain
 * @param tenantLogin
 * @param  {Function} callback
 */
SqlManager.prototype.getTenantIdbyDomain = function (tenantLogin, callback) {
    const self = this;
    self.pool.getConnection(function (err, connection) {
        if (err) {
            serviceLogger('error', `Error: ${err.message}`);
            return callback(err);
        }
        connection.query('SELECT `tenant_id` FROM `tbl_tenant` where `domain`= ?', [tenantLogin], function (err, rows) {
            if (err) {
                connection.release();
                serviceLogger('error', `Error: ${err.message}`);
                return callback(err);
            }
            connection.release();
            if (rows.length === 0) {
                return callback(null, null);
            }
            return callback(null, rows[0].tenant_id);
        });
    });
};


/**
 *  Get tenant domain by id
 * @param tenantId
 * @param callback
 * @returns {Promise}
 */
SqlManager.prototype.getTenantDomainById = function (tenantId, callback) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.pool.getConnection(function (err, connection) {
            if (err) {
                serviceLogger('error', `Error: ${err.message}`);
                return reject(err);
            }
            connection.query('SELECT `domain` FROM `tbl_tenant` where `tenant_id`= ?', [tenantId], function (err, rows) {
                if (err) {
                    connection.release();
                    serviceLogger('error', err.message);
                    return reject(err);
                }
                connection.release();
                if (rows.length === 0) {
                    return reject(new Error(`There no tenant with id: ${tenantId}`));
                }
                return resolve(rows[0].domain);
            });
        });
    });

};

/**
 * Get worker
 * @param tenantId
 * @param callsign
 * @param {Function} callback
 */
SqlManager.prototype.getWorker = function (tenantId, callsign, callback) {
    const self = this;
    self.pool.getConnection(function (err, connection) {
        if (err) {
            serviceLogger('error', `Error: ${err.message}`);
            return callback(err);
        }
        const selectVals = [
            callsign,
            tenantId
        ];
        connection.query('SELECT * FROM `tbl_worker` where callsign= ? and tenant_id= ?', selectVals, function (err, rows) {
            if (err) {
                serviceLogger('error', `Error: ${err.message}`);
                connection.release();
                return callback(err);
            }
            connection.release();
            const worker = rows.length ? rows[0] : null;
            return callback(null, worker);
        });
    });
};

/**
 * Get order details
 * @param orderId
 * @param {Function} callback
 */
SqlManager.prototype.getOrderDetails = function (orderId, callback) {
    const self = this;
    self.pool.getConnection(function (err, connection) {
        if (err) {
            serviceLogger('error', `Error: ${err.message}`);
            return callback(err);
        }
        connection.query('SELECT * FROM `tbl_order_detail_cost` where `order_id`= ?', [orderId], function (err, rows) {
            if (err) {
                serviceLogger('error', `Error: ${err.message}`);
                connection.release();
                return callback(err);
            }
            connection.release();
            const detailOrderData = rows.length ? rows[0] : {};
            return callback(null, detailOrderData);
        });
    });

};

/**
 * Get currency code by id
 * @param currencyId
 * @param callback
 */
SqlManager.prototype.getCurrencyCodeById = function (currencyId, callback) {
    const self = this;
    self.pool.getConnection(function (err, connection) {
        if (err) {
            serviceLogger('error', `Error: ${err.message}`);
            return callback(err);
        }
        connection.query('SELECT * FROM `tbl_currency` where `currency_id`= ?', [currencyId], function (err, rows) {
            if (err) {
                serviceLogger('error', `Error: ${err.message}`);
                connection.release();
                return callback(err);
            }
            connection.release();
            const tenantCurrency = rows.length ? rows[0].code : {};
            return callback(null, tenantCurrency);
        });
    })
};


/**
 * Get tenant currency pay
 * @param tenantId
 * @param cityId
 * @param {Function} callback
 */
SqlManager.prototype.getTenantCurrencyPay = function (tenantId, cityId, callback) {
    const self = this;
    self.pool.getConnection(function (err, connection) {
        if (err) {
            serviceLogger('error', `Error: ${err.message}`);
            return callback(err);
        }
        const selectVals = [tenantId, 'CURRENCY', cityId];
        connection.query('SELECT value as currency FROM `tbl_tenant_setting` where `tenant_id`= ? and `name`= ? and `city_id`= ?', selectVals, function (err, rows) {
            if (err) {
                serviceLogger('error', `Error: ${err.message}`);
                connection.release();
                return callback(err);
            }
            if (rows.length === 0) {
                connection.release();
                return callback(null, "");
            }
            const tenantCurrencyId = rows[0].currency;
            connection.query('SELECT * FROM `tbl_currency` where `currency_id`= ?', [tenantCurrencyId], function (err, rows) {
                if (err) {
                    serviceLogger('error', `Error: ${err.message}`);
                    connection.release();
                    return callback(err);
                }
                connection.release();
                const tenantCurrency = rows.length ? rows[0].code : {};
                return callback(null, tenantCurrency);
            });

        });
    });
};

/**
 * Get worker order push notification text
 * @param {Object} params
 * @param {number} params.tenantId
 * param {number} params.cityId
 * @param {number} params.positionId
 * @param {number} params.type
 * @returns {Promise<string>}
 */
SqlManager.prototype.getWorkerPushTemplate = function (params) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.pool.getConnection(function (err, connection) {
            if (err) {
                serviceLogger('error', `Error: ${err.message}`);
                return reject(err);
            }
            const queryParams = [params.tenantId, params.positionId, params.type, params.cityId];
            connection.query('SELECT text as push_text FROM `tbl_worker_push_notifications` where ' +
                '`tenant_id`= ? and ' +
                '`position_id`= ? and ' +
                '`type`= ? and ' +
                '`city_id`= ?', queryParams, function (err, rows) {
                if (err) {
                    serviceLogger('error', `Error: ${err.message}`);
                    connection.release();
                    return reject(err);
                }
                if (rows.length !== 0) {
                    connection.release();
                    return resolve(rows[0].push_text);
                }
                connection.query('SELECT text as push_text FROM `tbl_default_worker_push_notifications` ' +
                    'where `type`= ? ' +
                    'and `position_id` = ? ',
                    [params.type, params.positionId], function (err, rows) {
                        if (err) {
                            serviceLogger('error', `Error: ${err.message}`);
                            connection.release();
                            return reject(err);
                        }
                        connection.release();
                        const pushTemplate = rows.length ? rows[0].push_text : null;
                        return resolve(pushTemplate);
                    });
            });
        })
    });
};

/**
 * Get client order push notification text
 * @param {Object} params
 * @param {number} params.tenantId
 * @param {number} params.positionId
 * @param {number} params.type
 * @param {number} params.cityId
 * @param {Function} callback
 */
SqlManager.prototype.getClientPushTemplate = function (params, callback) {
    const self = this;
    self.pool.getConnection(function (err, connection) {
        if (err) {
            serviceLogger('error', `Error: ${err.message}`);
            return callback(err);
        }
        const queryParams = [params.tenantId, params.positionId, params.type, params.cityId];
        connection.query('SELECT text as push_text FROM `tbl_client_push_notifications` where ' +
            '`tenant_id`= ? and ' +
            '`position_id`= ? and ' +
            '`type`= ? and ' +
            '`city_id`= ?', queryParams, function (err, rows) {
            if (err) {
                serviceLogger('error', `Error: ${err.message}`);
                connection.release();
                return callback(err);
            }
            if (rows.length !== 0) {
                connection.release();
                return callback(null, rows[0].push_text);
            }
            connection.query('SELECT text as push_text FROM `tbl_default_client_push_notifications` ' +
                'where `type`= ? ' +
                'and `position_id` = ? ',
                [params.type, params.positionId], function (err, rows) {
                    if (err) {
                        serviceLogger('error', `Error: ${err.message}`);
                        connection.release();
                        return callback(err);
                    }
                    connection.release();
                    const pushTemplate = rows.length ? rows[0].push_text : null;
                    return callback(null, pushTemplate);
                });

        });
    });
};

/**
 * Get tenant company name
 * @param tenantId
 * @param {Function} callback
 */
SqlManager.prototype.getTenantCompanyName = function (tenantId, callback) {
    const self = this;
    self.pool.getConnection(function (err, connection) {
        if (err) {
            serviceLogger('error', `Error: ${err.message}`);
            return callback(err);
        }
        connection.query('SELECT company_name FROM `tbl_tenant` where `tenant_id`= ?', [tenantId], function (err, rows) {
            if (err) {
                serviceLogger('error', `Error: ${err.message}`);
                connection.release();
                return callback(err);
            }
            connection.release();
            const companyName = rows.length ? rows[0].company_name : {};
            return callback(null, companyName);
        });
    });
};
/**
 * Get client courier push template
 * @param statusId
 * @param {Function} callback
 */
SqlManager.prototype.getClientCourierPushTemplate = function (statusId, callback) {
    const self = this;
    self.pool.getConnection(function (err, connection) {
        if (err) {
            serviceLogger('error', `Error: ${err.message}`);
            return callback(null, {});
        }
        connection.query('SELECT text as push_text FROM `tbl_default_client_push_notifications_courier` where `type`= ?', [statusId], function (err, rows) {
            if (err) {
                serviceLogger('error', `Error: ${err.message}`);
                connection.release();
                return callback(err);
            }
            connection.release();
            const pushTemplate = rows.length ? rows[0].push_text : null;
            return callback(null, pushTemplate);
        });
    });
};


/**
 * Get android push api key (gcm api key)
 * @param typeApp  worker/client
 * @param  tenantId
 * @param  clientAppId
 * @param {Function} callback
 */
SqlManager.prototype.getAndroidPushApiKey = function (typeApp, tenantId, clientAppId, callback) {
    const self = this;
    self.pool.getConnection(function (err, connection) {
        if (err) {
            serviceLogger('error', `Error: ${err.message}`);
            return callback(err);
        }
        if (typeApp === "worker") {
            connection.query('SELECT value FROM `tbl_tenant_setting` where `name`= ? and `tenant_id` = ?', ["PUSH_ANDROID_WORKER", tenantId],
                function (err, rows) {
                    if (err) {
                        serviceLogger('error', `Error: ${err.message}`);
                        connection.release();
                        return callback(err);
                    }
                    connection.release();
                    const gcmKey = rows.length ? rows[0].value : null;
                    if (!gcmKey) {
                        return callback(new Error('No PUSH_ANDROID_WORKER'));
                    }
                    return callback(null, gcmKey);
                });
        } else if (typeApp === "client") {
            if (clientAppId) {
                connection.query('SELECT push_key_android FROM `tbl_mobile_app` where `tenant_id`= ? and `app_id`= ?', [tenantId, clientAppId],
                    function (err, rows) {
                        if (err) {
                            serviceLogger('error', `Error: ${err.message}`);
                            connection.release();
                            return callback(err);
                        }
                        connection.release();
                        const gcmKey = rows.length && rows[0].push_key_android ? rows[0].push_key_android : null;
                        if (!gcmKey) {
                            return callback(new Error('No push_key_android'));
                        }
                        return callback(null, gcmKey);
                    });
            } else {
                const clientAppDefaultName = 'Default';
                connection.query('SELECT push_key_android FROM `tbl_mobile_app` where `tenant_id`= ? and `name`= ?', [tenantId, clientAppDefaultName],
                    function (err, rows) {
                        if (err) {
                            serviceLogger('error', `Error: ${err.message}`);
                            connection.release();
                            return callback(err);
                        }
                        connection.release();
                        const gcmKey = rows.length && rows[0].push_key_android ? rows[0].push_key_android : null;
                        if (!gcmKey) {
                            return callback(new Error('No push_key_android'));
                        }
                        return callback(null, gcmKey);
                    });
            }
        } else {
            return callback(new Error("bad typeApp"));
        }

    });

};

/**
 * Get ios push api key (apns api key)
 * @param typeApp  worker/client
 * @param  tenantId
 * @param  clientAppId
 * @param {Function} callback
 */
SqlManager.prototype.getIosPushApiKey = function (typeApp, tenantId, clientAppId, callback) {
    const self = this;
    self.pool.getConnection(function (err, connection) {
        if (err) {
            serviceLogger('error', `Error: ${err.message}`);
            return callback(err);
        }
        if (typeApp === "worker") {
            let param;
            if (iosWorkerPushServerIsProd) {
                param = 'PUSH_IOS_WORKER_PRODUCTION';
            }
            else {
                param = 'PUSH_IOS_WORKER_SANDBOX';
            }
            connection.query('SELECT value FROM `tbl_default_settings` where `name`= ?', [param],
                function (err, rows) {
                    if (err) {
                        serviceLogger('error', `Error: ${err.message}`);
                        connection.release();
                        return callback(err);
                    }
                    connection.release();
                    const apnsKey = rows.length ? rows[0].value : null;
                    if (!apnsKey) {
                        return callback(new Error('No PUSH_IOS_WORKER'));
                    }
                    return callback(null, apnsKey);
                });
        } else if (typeApp === "client") {
            if (clientAppId) {
                connection.query('SELECT push_key_ios FROM `tbl_mobile_app` where `tenant_id`= ? and `app_id`= ?', [tenantId, clientAppId],
                    function (err, rows) {
                        if (err) {
                            serviceLogger('error', `Error: ${err.message}`);
                            connection.release();
                            return callback(err);
                        }
                        connection.release();
                        const apnsKey = rows.length && rows[0].push_key_ios ? rows[0].push_key_ios : null;
                        if (!apnsKey) {
                            return callback(new Error('No push_key_ios'));
                        }
                        return callback(null, apnsKey);
                    });
            } else {
                const clientAppDefaultName = 'Default';
                connection.query('SELECT push_key_ios FROM `tbl_mobile_app` where `tenant_id`= ? and `name`= ?', [tenantId, clientAppDefaultName],
                    function (err, rows) {
                        if (err) {
                            serviceLogger('error', `Error: ${err.message}`);
                            connection.release();
                            return callback(err);
                        }
                        connection.release();
                        const apnsKey = rows.length && rows[0].push_key_ios ? rows[0].push_key_ios : null;
                        if (!apnsKey) {
                            return callback(new Error('No push_key_ios'));
                        }
                        return callback(null, apnsKey);
                    });
            }
        } else {
            return callback(new Error("bad typeApp"));
        }
    });
};
/**
 * Get geo service api key
 * @param {Function} callback
 */
SqlManager.prototype.getGeoServiceApiKey = function (callback) {
    const self = this;
    self.pool.getConnection(function (err, connection) {
        if (err) {
            serviceLogger('error', `Error: ${err.message}`);
            return callback(err);
        }
        connection.query('SELECT value FROM `tbl_default_settings` where `name`= ?', ['GEOSERVICE_API_KEY'],
            function (err, rows) {
                if (err) {
                    serviceLogger('error', `Error: ${err.message}`);
                    connection.release();
                    return callback(err);
                }
                connection.release();
                const geoServiceApiKey = rows.length ? rows[0].value : null;
                return callback(null, geoServiceApiKey);
            });
    });
};

/**
 * Get array of workers
 * @param tenantId
 * @param workerCallsignArr [7,8,4,13]
 * @param callback
 */
SqlManager.prototype.getListedWorkers = function (tenantId, workerCallsignArr, callback) {
    const self = this;
    self.pool.getConnection(function (err, connection) {
        if (err) {
            serviceLogger('error', `Error: ${err.message}`);
            return callback(err);
        }
        const selectVals = [tenantId];
        const workerCallsignString = workerCallsignArr.join(',');
        connection.query('SELECT * FROM `tbl_worker` where `tenant_id`= ? and callsign in (' + workerCallsignString + ')', selectVals,
            function (err, rows) {
                if (err) {
                    serviceLogger('error', `Error: ${err.message}`);
                    connection.release();
                    return callback(err);
                }
                connection.release();
                const workers = rows.length ? rows : null;
                return callback(null, workers);
            });
    });
};

/**
 * Get array of workers in city and not in array of callsigns
 * @param tenantId
 * @param cityId
 * @param workerCallsignArr [7,8,4,13]
 * @param callback
 */
SqlManager.prototype.getNotListedWorkersInCity = function (tenantId, cityId, workerCallsignArr, callback) {
    const self = this;
    self.pool.getConnection(function (err, connection) {
        if (err) {
            serviceLogger('error', `Error: ${err.message}`);
            return callback(err);
        }
        let workerCallsignString = workerCallsignArr.join(',');
        if (workerCallsignString.length < 1) {
            workerCallsignString = "";
        }
        const selectVals = [tenantId, cityId, workerCallsignString];

        connection.query('SELECT * FROM `tbl_worker` as w JOIN `tbl_worker_has_city` as whc  ' +
            'on `whc`.`worker_id` = `w`.`worker_id` ' +
            'where `w`.`tenant_id`= ? ' +
            'and `whc`.`city_id` = ? ' +
            'and `w`.callsign  ' +
            'not in (?) ' +
            'and `w`.`block`=0 ' +
            'and `w`.`activate`=1', selectVals,
            function (err, rows) {
                if (err) {
                    serviceLogger('error', `Error: ${err.message}`);
                    connection.release();
                    return callback(err);
                }
                connection.release();
                const workers = rows.length ? rows : null;
                return callback(null, workers);
            });
    });
};


/**
 * get workers in city
 * @param tenantId
 * @param cityId
 * @param callback
 */
SqlManager.prototype.getWorkersInCity = function (tenantId, cityId, callback) {
    const self = this;
    self.pool.getConnection(function (err, connection) {
        if (err) {
            serviceLogger('error', `Error: ${err.message}`);
            return callback(err);
        }
        const selectVals = [tenantId, cityId];
        connection.query('SELECT * FROM `tbl_worker` where `tenant_id`= ? and `city_id`= ?  and block=0', selectVals,
            function (err, rows) {
                if (err) {
                    serviceLogger('error', `Error: ${err.message}`);
                    connection.release();
                    return callback(err);
                }
                connection.release();
                const workers = rows.length ? rows : null;
                return callback(null, workers);
            });
    });

};


/**
 * Get selected workers devices
 * @param workerIdsArr
 * @param callback
 */
SqlManager.prototype.getSelectedWorkersDevices = function (workerIdsArr, callback) {
    const self = this;
    self.pool.getConnection(function (err, connection) {
        if (err) {
            serviceLogger('error', `Error: ${err.message}`);
            return callback(err);
        }
        const workerIdsString = workerIdsArr.join(',');
        connection.query('SELECT w.worker_id, w.device, w.device_token, w.callsign, whc.city_id FROM `tbl_worker` w ' +
            'join `tbl_worker_has_city` whc on w.worker_id = whc.worker_id ' +
            'where w.worker_id in (' + workerIdsString + ') and whc.last_active=1 and w.device_token IS NOT NULL', null,
            function (err, rows) {
                if (err) {
                    serviceLogger('error', `Error: ${err.message}`);
                    connection.release();
                    return callback(err);
                }
                connection.release();
                const devices = rows.length ? rows : null;
                return callback(null, devices);
            });
    })
};

/**
 * Get selected client devices
 * @param clientIdsArr
 * @param callback
 */
SqlManager.prototype.getSelectedClientsDevices = function (clientIdsArr, callback) {
    const self = this;
    self.pool.getConnection(function (err, connection) {
        if (err) {
            serviceLogger('error', `Error: ${err.message}`);
            return callback(err);
        }
        const clientIdsString = clientIdsArr.join(',');
        connection.query('SELECT client_id,device,device_token,app_id FROM `tbl_client` where client_id in (' + clientIdsString + ') and device_token IS NOT NULL', null,
            function (err, rows) {
                if (err) {
                    serviceLogger('error', `Error: ${err.message}`);
                    connection.release();
                    return callback(err);
                }
                connection.release();
                const devices = rows.length ? rows : null;
                return callback(null, devices);
            });
    })
};

/**
 * Check insert to db
 * @param key
 * @param value
 * @return {Promise}
 */
SqlManager.prototype.checkInsert = function (key, value) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.pool.getConnection((err, connection) => {
            if (err) {
                return reject(err);
            }
            const insertVals = [key, value];
            connection.query("INSERT INTO `tbl_check` set `key` = ?, `value` = ?", insertVals,
                (err, result) => {
                    if (err) {
                        connection.release();
                        return reject(err);
                    } else {
                        connection.release();
                        resolve();
                    }
                });
        });
    });
};

/**
 * Check select from db
 * @param key
 * @return {Promise}
 */
SqlManager.prototype.checkSelect = function (key) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.pool.getConnection((err, connection) => {
            if (err) {
                return reject(err);
            }
            const selectVals = [key];
            connection.query("SELECT * from `tbl_check` where `key` = ?", selectVals,
                (err, result) => {
                    if (err) {
                        connection.release();
                        return reject(err);
                    } else {
                        connection.release();
                        return resolve(result);
                    }
                });
        });
    });
};

/**
 * Check update to db
 * @param key
 * @param value
 * @return {Promise}
 */
SqlManager.prototype.checkUpdate = function (key, value) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.pool.getConnection((err, connection) => {
            if (err) {
                return reject(err);
            }
            const updateVals = [value, key];
            connection.query('UPDATE  `tbl_check` SET `value` = ? where `key` = ?', updateVals,
                (err, result) => {
                    if (err) {
                        connection.release();
                        return reject(err);
                    } else {
                        connection.release();
                        return resolve();
                    }
                });
        });
    });
};

/**
 * Check delete from db
 * @param key
 * @return {Promise}
 */
SqlManager.prototype.checkDelete = function (key) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.pool.getConnection((err, connection) => {
            if (err) {
                return reject(err);
            }
            const updateVals = [key];
            connection.query('DELETE from `tbl_check` where `key` = ?', updateVals,
                (err, result) => {
                    if (err) {
                        connection.release();
                        return reject(err);
                    } else {
                        connection.release();
                        return resolve();
                    }
                });
        });
    });
};


module.exports = exports = new SqlManager();

