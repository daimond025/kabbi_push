"use strict";

/**
 * Strategy for  retry connect to Redis when reconnect event
 * @param {Object} options
 * @returns {Number|Error}
 */
module.exports = function (options) {
    if (options.error.code === 'ECONNRESET') {
        return new Error('The server reset the connection at redisClient');
    }
    if (options.error.code === 'ETIMEDOUT') {
        return new Error('The server timeouted the connection at redisClient');
    }
    if (options.error.code === 'ECONNREFUSED') {
        // End reconnecting on a specific error and flush all commands with a individual error
        return new Error('The server refused the connection at redisClient');
    }
    if (options.total_retry_time > 1000 * 60 * 60) {
        // End reconnecting after a specific timeout and flush all commands with a individual error
        return new Error('Retry time exhausted at redisClient');
    }
    if (options.times_connected > 10) {
        // End reconnecting with built in error
        return new Error('Retry attempts ended at redisClient');
    }
    // reconnect after
    return Math.max(options.attempt * 100, 2000);
};
