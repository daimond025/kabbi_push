'use strict';
const mongoose = require('mongoose');
const config = require("../services/lib").getConfig();
const serviceLogger = require('../services/logger').serviceLogger;


const mongoUrl = config.MONGODB_MAIN_DSN;
const options = {
    server: {
        socketOptions: {
            autoReconnect: true,
            keepAlive: 300000,
            connectTimeoutMS: 30000
        },
        poolSize: 100,
        reconnectTries: Number.MAX_VALUE
    },
  /*  replset: {
        socketOptions: {
            keepAlive: 300000,
            connectTimeoutMS: 30000
        }
    }*/
};


const options_2 = {
    autoReconnect: true,
    keepAlive: 300000,
    connectTimeoutMS: 30000,
    reconnectTries:  Number.MAX_VALUE,
    useMongoClient: true,
    poolSize: 100,
};

mongoose.Promise = global.Promise;
mongoose.connect(mongoUrl, options_2);

const db = mongoose.connection;
db.setMaxListeners(50);

db.on('error', (err) => {
    serviceLogger('error', 'connection error:', err.message);
});

db.on('disconnected', () => {
    serviceLogger('error', 'Mongoose default connection disconnected');
});

db.once('open', () => {
    serviceLogger("info", "Connected to MongoDB at " + mongoUrl);
});

const Schema = mongoose.Schema;

// Schemas


const Chat = new Schema({
    room_id: {type: String},
    is_private: {type: Boolean},
    is_readed: {type: Boolean},
    is_bulk_message: {type: Boolean},
    city_id: {type: Number},
    tenant_id: {type: Number},
    tenant_domain: {type: String},
    receiver_id: {type: Number},
    receiver_type: {type: String},
    sender_type: {type: String},
    sender_role: {type: String},
    sender_id: {type: Number},
    sender_f: {type: String},
    sender_i: {type: String},
    sender_o: {type: String},
    sender_time: {type: String},
    timestamp: {type: Number},
    message: {type: String},
    uuid: {type: String},
    modified: {type: Date, default: Date.now}
});

const BulkNotify = new Schema({
    notification_id: {type: Number},
    tenant_id: {type: Number},
    city_ids: {type: String},
    app_id: {type: Number},
    client_type: {type: String},
    client_ids: {type: String},
    title: {type: String},
    message: {type: String},
    created_at: {type: Number},
    report: {
        ios: {
            success: Number,
            failure: Number,
            results: [
                {
                    user_id: Number,
                    token: String,
                    error: String,
                    result: Number

                }
            ]
        },
        android: {
            success: Number,
            failure: Number,
            results: [
                {
                    user_id: Number,
                    token: String,
                    error: String,
                    result: Number

                }
            ]
        }
    }
}, {collection: 'bulk_notify'});

const Check = new Schema({
    key: {type: String},
    value: {type: String},
}, {collection: 'check'});


const BulkNotifyModel = mongoose.model('bulk_notify', BulkNotify);
const CheckModel = mongoose.model('check', Check);
const ChatModel = mongoose.model('chat', Chat);

/**
 * check insert
 * @param key
 * @param value
 * @return {Promise}
 */
function checkInsert(key, value) {
    return new Promise((resolve, reject) => {
        const check = new CheckModel({
            key: key,
            value: value,
        });
        check.save((err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

/**
 * check select
 * @param key
 * @return {Promise}
 */
function checkSelect(key) {
    return new Promise((resolve, reject) => {
        CheckModel.findOne({'key': key}, (err, row) => {
            if (err) {
                reject(err);
            } else {
                if (row) {
                    resolve(row);
                } else {
                    reject(new Error(`mongodb: can not select row for key: ${key}`))
                }
            }
        });
    });

}

/**
 * check update
 * @param key
 * @param value
 * @return {Promise}
 */
function checkUpdate(key, value) {
    return new Promise((resolve, reject) => {
        CheckModel.findOneAndUpdate({key: key}, {$set: {value: value}}, (err, row) => {
            if (err) {
                reject(err);
            } else {
                if (row) {
                    resolve(row);
                } else {
                    reject(new Error(`mongodb: can not update row for key: ${key} with value: ${value}`));
                }

            }
        });
    });

}
/**
 * check delete
 * @param key
 * @return {Promise}
 */
function checkDelete(key) {
    return new Promise((resolve, reject) => {
        CheckModel.findOneAndRemove({key: key}, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });

}

module.exports = {
    BulkNotifyModel,
    ChatModel,
    checkInsert,
    checkSelect,
    checkUpdate,
    checkDelete,
};
