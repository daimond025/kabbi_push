'use strict';

const serviceLogger = require('../../services/logger').serviceLogger;
const bulkNotifyLogger = require('../../services/logger').bulkNotifyLogger;
const pusher = require('../../services/pusher/pushService');
const sqlManager = require('../../dbs/sqlManager');
const bulkNotifyReport = require('../../services/report/bulkNotifyReport');
const xssFilters = require('xss-filters');
const ChatModel = require('../../dbs/mongoManager').ChatModel;
const uuid = require('uuid/v4');

module.exports = function controller(req, res, next) {
    serviceLogger('info', "Called method: bulk_notify");
    serviceLogger('info', req.body);

    req.checkBody({
        'notification_id': {
            notEmpty: true,
            errorMessage: 'Invalid notification_id'
        },
        'tenant_id': {
            notEmpty: true,
            errorMessage: 'Invalid tenant_id'
        },
        'city_ids': {
            notEmpty: true,
            errorMessage: 'Invalid city_ids'
        },
        'client_type': {
            notEmpty: true,
            errorMessage: 'Invalid client_type'
        },
        'client_ids': {
            notEmpty: true,
            errorMessage: 'Invalid client_ids'
        },
        'sender_id': {
            notEmpty: true,
            errorMessage: 'Invalid sender_id'
        },
        'sender_role': {
            notEmpty: true,
            errorMessage: 'Invalid sender_role'
        },
        'sender_last_name': {
            isLength: {
                options: [{min: 0, max: 300}],
                errorMessage: 'Must be between 1 and 300 chars long'
            },
            errorMessage: 'Invalid sender_last_name'
        },
        'sender_name': {
            isLength: {
                options: [{min: 0, max: 300}],
                errorMessage: 'Must be between 1 and 300 chars long'
            },
            errorMessage: 'Invalid sender_name'
        },
        'sender_second_name': {
            isLength: {
                options: [{min: 0, max: 300}],
                errorMessage: 'Must be between 1 and 300 chars long'
            },
            errorMessage: 'Invalid sender_second_name'
        },
        'title': {
            isLength: {
                options: [{min: 1, max: 100}],
                errorMessage: 'Must be between 1 and 100 chars long'
            },
            errorMessage: 'Invalid title'
        },
        'message': {
            isLength: {
                options: [{min: 1, max: 200}],
                errorMessage: 'Must be between 1 and 200  chars long'
            },
            errorMessage: 'Invalid message'
        },
    });



    req.getValidationResult()
        .then((validationResult) => {

            try {
                validationResult.throw();
            } catch (err) {
                let error = err.array({onlyFirstError: true});
                res.status(400);
                res.send(error[0]);
                return;
            }

            const answer = '{"result":1}';
             res.write(answer);
             res.end();
            const notificationId = parseInt(req.body.notification_id);
            const tenantId = parseInt(req.body.tenant_id);
            const clientType = req.body.client_type;
            const clientIds = req.body.client_ids;
            const cityIds = req.body.city_ids;
            const senderId = req.body.sender_id;
            const senderRole = req.body.sender_role;
            const senderLastName = req.body.sender_last_name;
            const senderName = req.body.sender_name;
            const senderSecondName = req.body.sender_second_name;
            const clientIdsArr = clientIds.split(',');
            const title = xssFilters.inHTMLData(req.body.title);
            const message = xssFilters.inHTMLData(req.body.message);
            const createdAt = Math.floor(Date.now() / 1000);
            bulkNotifyLogger(notificationId, 'info', 'Called method: bulk_notify');
            bulkNotifyLogger(notificationId, 'info', req.body);

            getDevices(clientIdsArr, clientType)
                .then(devices => {

                    serviceLogger('info', "devices:");
                    serviceLogger('info', devices);

                    if (Array.isArray(devices)) {
                        const settings = {
                            title: title,
                            message: message,
                            contentAvailable: null,
                            actionLocKey: null,
                            category: null,
                            icon: null,
                            sound: 'default',
                            command: 'bulk_notify',
                            command_params: {
                                'notification_id': notificationId
                            },

                        };
                        const clientApps = {};

                        for (let deviceData of devices) {
                            let appId;
                            if (clientType === "client") {
                                appId = parseInt(deviceData.app_id);
                            } else {
                                appId = 1;
                            }
                            if (!clientApps.hasOwnProperty(appId)
                                && ["IOS", "ANDROID"].indexOf(deviceData.device) !== -1
                                && deviceData.device_token) {
                                clientApps[appId] = {
                                    'ios': [],
                                    'android': []
                                };
                            }
                            if (deviceData.device === "IOS" && deviceData.device_token) {
                                clientApps[appId].ios.push(deviceData);
                            }
                            if (deviceData.device === "ANDROID" && deviceData.device_token) {
                                clientApps[appId].android.push(deviceData);
                            }
                        }


                        for (let appId in clientApps) {
                            appId = parseInt(appId);
                            let androidDevices = clientApps[appId].android;
                            let androidTokens = androidDevices.map(android => {
                                return android.device_token;
                            });
                            let iosDevices = clientApps[appId].ios;
                            let iosTokens = iosDevices.map(ios => {
                                return ios.device_token;
                            });
                            let appDevices = [];
                            let reportParams = {
                                notification_id: notificationId,
                                tenant_id: tenantId,
                                sender_second_name: senderSecondName,
                                sender_name: senderName,
                                sender_last_name: senderLastName,
                                sender_id: senderId,
                                sender_role: senderRole,
                                city_ids: cityIds,
                                app_id: appId,
                                client_type: clientType,
                                client_ids: [],
                                title: title,
                                message: message,
                                created_at: createdAt
                            };
                            let results = {'android': null, 'ios': null};

                            pusher.sendPush(tenantId, 'android', androidTokens, clientType, appId, settings)
                                .then(androidRes => {
                                    results.android = androidRes;
                                    appDevices = appDevices.concat(androidDevices);
                                    reportParams.client_ids = reportParams.client_ids.concat(androidDevices.map(android => {
                                        return android.client_id ? android.client_id : android.worker_id;
                                    }));
                                    return pusher.sendPush(tenantId, 'ios', iosTokens, clientType, appId, settings);
                                })
                                .then(iosRes => {
                                    results.ios = iosRes;
                                    appDevices = appDevices.concat(iosDevices);
                                    bulkNotifyLogger(notificationId, 'info', `Bulk notify results: ${JSON.stringify(results)}`);
                                    reportParams.app_id = reportParams.app_id ? reportParams.app_id : 1;
                                    reportParams.client_ids = reportParams.client_ids.concat(iosDevices.map(ios => {
                                        return ios.client_id ? ios.client_id : ios.worker_id;
                                    }));
                                    reportParams.client_ids = reportParams.client_ids.join(',');
                                    bulkNotifyReport(reportParams, appDevices, results);

                                })
                                .catch(error => {
                                    bulkNotifyLogger(notificationId, 'info', `Bulk notify error: ${error.message || error}`);
                                    reportParams.app_id = reportParams.app_id ? reportParams.app_id : 1;
                                    reportParams.client_ids = reportParams.client_ids.join(',');
                                    bulkNotifyReport(reportParams, appDevices, results);
                                })
                                .then(() => {
                                    if (clientType === "worker") {
                                        saveBulkMessageToPersonalWorkerChat(reportParams, devices);
                                    }
                                })
                        }
                    } else {
                        bulkNotifyLogger(notificationId, 'info', 'bulk_notify->bad devices:');
                        bulkNotifyLogger(notificationId, 'info', devices);
                    }
                })
                .catch(err => {
                    bulkNotifyLogger(notificationId, 'error', 'bulk_notify->getDevices:error');
                    bulkNotifyLogger(notificationId, 'error', err.message);
                })
        });


};

/**
 * Get mobile devices by ids and client type(worker/client)
 * @param idsArr
 * @param clientType
 * @returns {Promise}
 */
function getDevices(idsArr, clientType) {
    return new Promise(function (resolve, reject) {
        if (clientType === "client") {
            sqlManager.getSelectedClientsDevices(idsArr, (err, devices) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(devices);
                }
            })
        } else if (clientType === "worker") {
            sqlManager.getSelectedWorkersDevices(idsArr, (err, devices) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(devices);
                }
            })
        } else {
            reject(`Bad client type:${clientType}`);
        }
    })
}

function saveBulkMessageToPersonalWorkerChat(params, devices) {
    sqlManager.getTenantDomainById(params.tenant_id)
        .then(tenantDomain => {
            serviceLogger('info', `bulkNotify->saveBulkMessageToPersonalWorkerChat->sqlManager.getTenantDomainById->tenantDomain: ${tenantDomain}`);
            for (let workerData of devices) {
                const chatMessage = new ChatModel({
                    room_id: `${tenantDomain}_worker_${workerData.callsign}`,
                    is_private: false,
                    is_bulk_message: true,
                    is_readed: true,
                    tenant_id: params.tenant_id,
                    tenant_domain: tenantDomain,
                    city_id: workerData.city_id,
                    receiver_id: workerData.city_id,
                    receiver_type: "worker",
                    sender_type: "user",
                    sender_role: params.sender_role,
                    sender_id: params.sender_id,
                    sender_f: params.sender_last_name,
                    sender_i: params.sender_name,
                    sender_o: params.sender_second_name,
                    sender_time: getDateTime(),
                    timestamp: Date.now(),
                    message: params.title + '. ' + params.message,
                    uuid: uuid()
                });
                chatMessage.save((err) => {
                    if (err) {
                        serviceLogger('error', `bulkNotify->saveBulkMessageToPersonalWorkerChat->chatMessage.save->Error: ${err.message}`);
                    } else {
                        serviceLogger('info', `bulkNotify->saveBulkMessageToPersonalWorkerChat->chatMessage.save->Result: true`);
                    }
                });
            }
        })
        .catch(err => {
            serviceLogger('error', `bulkNotify->saveBulkMessageToPersonalWorkerChat->chatMessage.save->sqlManager.getTenantDomainById->Error: ${err.message}`);
        });
}

function getDateTime() {
    const now = new Date();
    let month = now.getMonth() + 1;
    let day = now.getDate();
    let hour = now.getHours();
    let minute = now.getMinutes();
    if (month.toString().length === 1) {
        month = '0' + month;
    }
    if (day.toString().length === 1) {
        day = '0' + day;
    }
    if (hour.toString().length === 1) {
        hour = '0' + hour;
    }
    if (minute.toString().length === 1) {
        minute = '0' + minute;
    }
    return day + '.' + month + ' ' + hour + ':' + minute;
}


