'use strict';

const serviceLogger = require('../../services/logger').serviceLogger;
const pusher = require('../../services/pusher/pushService');

/**
 * Send push to client app with info about change balance of money or bonuse
 * TYPE REQUEST: POST
 * @param req
 * req.body:
 *   tenant_id  -    68
 *   app_id - 1
 *   tenant_login   - 3colors
 *   balance    -  1000
 *   currency   - RUB
 *   bonus_balance - 12
 *   device - android|ios
 *   token  - 12easdgdfgae12easdfasd
 *   lang    - ru/en/se...
 * @param res
 */
// @NOTE: URL has been changed from send_push_transaction to client_send_push_transaction
module.exports = function controller(req, res) {
    serviceLogger('info', "Called method: client_send_push_transaction");
    serviceLogger('info', req.body);
    const answer = '{"result":1}';
    res.write(answer);
    res.end();
    const tenantId = parseInt(req.body.tenant_id);
    const clientAppId = parseInt(req.body.app_id);
    const tenantLogin = req.body.tenant_login;
    const balance = req.body.balance;
    const bonusBalance = req.body.bonus_balance;
    const currency = req.body.currency;
    const typeDevice = req.body.device;
    const deviceToken = req.body.token;
    const lang = req.body.lang;
    req.i18n.setLocale(lang);
    const command = "new_balance";
    const contentAvailable = null;
    const actionLocKey = null;
    const category = null;
    const sound = 'default';
    const BalancePhrase = req.i18n.__("Your balance");
    const command_params = {
        'balance': balance,
        'bonus_balance': bonusBalance,
        'currency': currency,
        'tenant_login': tenantLogin
    };
    const BonusPhrase = req.i18n.__("bonuses");
    const title = BalancePhrase + ' ' + balance + ' ' + currency + '/ ' + bonusBalance + ' ' + BonusPhrase;
    const settings = {
        title: title,
        message: title,
        contentAvailable: contentAvailable,
        actionLocKey: actionLocKey,
        category: category,
        icon: null,
        sound: sound,
        command: command,
        command_params: command_params,
    };
    if (typeof typeDevice === "string" && typeDevice.length > 0 && typeof deviceToken === "string" && deviceToken.length > 0) {
        const typeClientDevice = typeDevice.toLowerCase();
        pusher.sendPush(tenantId, typeClientDevice, deviceToken, "client", clientAppId, settings)
            .then(result => {
                serviceLogger('info', `${typeClientDevice} push result: ${JSON.stringify(result)}`);
            }).catch(err => {
            serviceLogger('error', `${typeClientDevice} push error: ${JSON.stringify(err)}`);
        });
    } else {
        serviceLogger('error', `BAD typeDevice or deviceToken. typeDevice:${typeDevice}; deviceToken:${deviceToken}`);
    }
}

