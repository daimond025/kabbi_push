'use strict';

const serviceLogger = require('../../services/logger').serviceLogger;
const pusher = require('../../services/pusher/pushService');

/**
 * destroy AndroidClientAppSenderService
 * Using when updates gsm key for client app
 * @param req
 *  req.query:
 *    tenant_id
 *    app_id
 * @param res
 */
module.exports = function controller(req, res) {
    serviceLogger('info', "Called method: destroy_android_client_app_sender_service");
    serviceLogger('info', req.query);
    const appId = req.query.app_id;
    const result = pusher.destroyAndroidClientAppSenderService(appId);
    process.send({
        cmd: 'destroy_android_client_app_sender_service',
        app_id: appId
    });
    let answer = '{"result":0}';
    if (result === true) {
        answer = '{"result":1}';
        res.write(answer);
        res.end();
    } else {
        res.write(answer);
        res.end();
    }
}


