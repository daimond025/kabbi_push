"use strict";

const PHPUnserialize = require('php-unserialize');
const serviceLogger = require('../../services/logger').serviceLogger;
const orderLogger = require('../../services/logger').orderLogger;
const async = require('async');
const redisOrdersManager = require('../../dbs/redisOrderManager');
const sqlManager = require('../../dbs/sqlManager');
const pusher = require('../../services/pusher/pushService');
const positionInfo = require('../../services/workerPositionManager').positionInfo;
const positionManager = require('../../services/workerPositionManager');
const statusLabelTranslation = require('../../services/statusLabelTranslation');
const statusInfo = require('../../services/orderStatusManager').statusInfo;

/**
 * Send push to client to notify about changing order status
 * TYPE REQUEST: GET
 * @param req
 * req.body:
 *  order_id
 *  position_id
 *  city_id
 *  tenant_id
 *  device_token
 *  type_device
 *  status_group
 *  status_id
 *  lang
 *  visible 1|0
 * @param res
 */
module.exports = function controller(req, res) {
    //Классы авто которые не являются, такси, для них посылаем спец пуши
    // Это временное решение
    const noTaxiCarClasses = ["26", "27", "28"];
    let isTaxiOrder = true;
    serviceLogger('info', "Called method: send_push_notification_for_client");
    serviceLogger('info', req.query);
    let answer = '{"result":1}';
    res.write(answer);
    res.end();
    let orderId = req.query.order_id;
    let positionId = parseInt(req.query.position_id);
    let tenantId = parseInt(req.query.tenant_id);
    const clientAppId = parseInt(req.query.app_id);
    let deviceToken = req.query.device_token;
    let typeDevice = req.query.type_device;
    if (typeof typeDevice === "string") {
        typeDevice = typeDevice.toUpperCase();
    }
    let statusGroup = req.query.status_group;
    let statusId = parseInt(req.query.status_id);
    let lang = req.query.lang;
    let cityId = parseInt(req.query.city_id);
    let visible = req.query.visible;
    let hasCar = positionManager.positionHasCar(positionId);
    serviceLogger('info', 'hascar: ' + hasCar);
    req.i18n.setLocale(lang);
    async.parallel([
        function getOrderForId(callback) {
            redisOrdersManager.getOrder(tenantId, orderId, (err, orderItem) => {
                if (err) {
                    serviceLogger("info", "Cant no get order form redisDb");
                    serviceLogger("info", err);
                } else if (orderItem) {
                    //временный костыль для курьеров которые на основе такси сделанны
                    if (positionId === positionInfo.simple_taxi_driver.position_id) {
                        const tariffCarClassId = orderItem.tariff.class_id;
                        if (noTaxiCarClasses.indexOf(tariffCarClassId) !== -1) {
                            serviceLogger("error", "It's not taxi order");
                            isTaxiOrder = false;
                        }
                        return callback(null, orderItem);
                    } else {
                        return callback(null, orderItem);
                    }
                }
                return callback(null, null);
            });
        },
        function getTenantCurrency(callback) {
            sqlManager.getTenantCurrencyPay(tenantId, cityId, (err, tenantCurrency) => {
                if (err) {
                    serviceLogger("error", "Error of getTenantCurrencyPay");
                    serviceLogger("error", err);
                    callback(null, null);
                } else {
                    callback(null, tenantCurrency);
                }

            });
        },
        function getOrderDetailCost(callback) {
            sqlManager.getOrderDetails(orderId, (err, detailOrderData) => {
                if (err) {
                    serviceLogger("error", "Error of getTenantCurrencyPay");
                    serviceLogger("error", err);
                    callback(null, null);
                } else {
                    callback(null, detailOrderData);
                }
            });
        },
        function getPushTemplate(callback) {
            sqlManager.getClientPushTemplate({
                    tenantId: tenantId,
                    positionId: positionId,
                    type: statusId,
                    cityId: cityId
                },
                (err, pushTemplate) => {
                    if (err) {
                        serviceLogger("error", "Error of getClientPushTemplate");
                        serviceLogger("error", err);
                        callback(null, null);
                    } else {
                        callback(null, pushTemplate);
                    }
                });
        },
        function getTenantCompanyName(callback) {
            sqlManager.getTenantCompanyName(tenantId, (err, tenantCompanyName) => {
                if (err) {
                    serviceLogger("error", "Error of getTenantCompanyName");
                    serviceLogger("error", err);
                    callback(null, null);
                } else {
                    callback(null, tenantCompanyName);
                }
            });
        },
        //@todo it's temp function for getting courier template
        function getPushTempalteCourier(callback) {
            sqlManager.getClientCourierPushTemplate(statusId, (err, pushTemplate) => {
                if (err) {
                    serviceLogger("error", "Error of getClientCourierPushTemplate");
                    serviceLogger("error", err);
                    callback(null, null);
                } else {
                    callback(null, pushTemplate);
                }
            });
        }
    ], (err, results) => {
        if (err) {
            serviceLogger('error', err);
            return;
        }
        //Задаем переменыне
        let brand = '',
            color = '',
            model = '',
            number = '',
            address = '',
            datetime = '',
            time = '',
            currency = '',
            price = '',
            companyName = '',
            title = '',
            message = '',
            pushText = '',
            statusLabel = '';

        //Инфа заказа
        const orderData = results[0];
        if (orderData && Object.keys(orderData).length !== 0) {
            statusId = orderData.status.status_id;
            statusGroup = orderData.status.status_group;
            statusLabel = orderData.status.status_label;
            if (Array.isArray(orderData.car) || (typeof orderData.car === "object" && orderData.car !== null)) {
                if (typeof  orderData.car.color === "string") {
                    try {
                        color = orderData.car.color;
                        color = color.toLowerCase();
                        color = req.i18n.__(color);
                    } catch (err) {
                        serviceLogger("error", err.message);
                    }
                }
                number = orderData.car.gos_number ? orderData.car.gos_number : '';
                let carName = orderData.car.name ? orderData.car.name : '';
                carName = carName.split(' ');
                brand = carName[0] ? carName[0] : '';
                model = carName[1] ? carName[1] : '';
            } else if (isOrderProcessedThroughTheExternalExchange(orderData)) {
                const carData = getCarInfoFromExternalExchange(orderData);
                if (carData) {
                    try {
                        color = carData.color;
                        color = color.toLowerCase();
                        color = req.i18n.__(color);
                        number = carData.number;
                        brand = carData.brand;
                        model = carData.model;
                    } catch (err) {
                        serviceLogger("error", err.message);
                    }
                }
            }
            if (orderData.time_to_client) {
                time = orderData.time_to_client;
            }
            if (orderData.order_time) {
                const timeOrder = parseInt(orderData.order_time) * 1000;
                const date = new Date(timeOrder);
                // Hours part from the timestamp
                const hours = date.getHours();
                // Minutes part from the timestamp
                const minutes = "0" + date.getMinutes();
                // Seconds part from the timestamp
                const seconds = "0" + date.getSeconds();
                // Will display time in 10:30:23 format
                datetime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
            }
            if (orderData.address) {
                try {
                    const addressNew = PHPUnserialize.unserialize(orderData.address);
                    if (addressNew && addressNew.A) {
                        address = addressNew.A.city + ", " + addressNew.A.street + ", " + addressNew.A.house;
                    }
                } catch (err) {
                    serviceLogger('error', err);
                }
            }
        }
        //Валюта
        if (results[1]) {
            currency = results[1];
        }
        //Стоимость заказа
        if (results[2]) {
            const costData = results[2];
            if (typeof costData === "object" && costData !== null) {
                if (costData.summary_cost) {
                    price = costData.summary_cost;
                }
            }
        }
        if (results[4]) {
            companyName = results[4];
        }
        //@todo Верменная штука для курьеров, пото удалить
        let templateData = "";
        if (isTaxiOrder) {
            //Шаблон сообщения
            templateData = results[3];
        } else {
            templateData = results[5];
        }
        if (templateData) {
            pushText = templateData;
            const templateArr = {
                '#TAXI_NAME#': companyName,
                '#ADDRESS#': address,
                '#BRAND#': brand,
                '#MODEL#': model,
                '#COLOR#': color,
                '#NUMBER#': number,
                '#TIME#': time,
                '#PRICE#': price,
                '#CURRENCY#': currency,
                '#DATETIME#': datetime
            };
            for (let key in templateArr) {
                pushText = pushText.replace(key, templateArr[key]);
            }
        }
        title = null;
        message = null;
        pushText = (pushText === null) ? "" : pushText.toString();
        if (visible === 0) {
            title = null;
            message = null;
        } else {
            if (typeDevice === "IOS") {
                title = "";
                message = pushText;
            }
            if (typeDevice === "ANDROID") {
                title = pushText;
                message = pushText;
            }
        }
        let sound = 'default';
        let command = "update_order";
        let command_params = {
            'order_id': orderId.toString()
        };

        let worker = "";
        let workerPhone = "";
        let carDescr = "";
        if (hasCar) {
            carDescr = brand + " " + model + " " + number + " " + color;
        }
        if (orderData && orderData.worker) {
            worker = orderData.worker.name + " " + orderData.worker.second_name;
            workerPhone = "+" + orderData.worker.phone;
        }
        switch (statusGroup) {
            case "new":
                if (isTaxiOrder) {
                    statusLabel = statusLabelTranslation(req, statusGroup, statusId, positionId);
                } else {
                    statusLabel = req.i18n.__('Search courier...');
                }
                command_params = {
                    'order_id': orderId.toString(),
                    'status': statusGroup.toString(),
                    'status_label': statusLabel.toString()
                };
                break;
            case "pre_order":
                if (statusId === statusInfo.new_pre_order.status_id || statusId === statusInfo.new_pre_order_no_parking.status_id) {
                    statusLabel = statusLabelTranslation(req, statusGroup, statusId, positionId);
                    command_params = {
                        'order_id': orderId.toString(),
                        'status': statusGroup.toString(),
                        'status_label': statusLabel.toString()
                    };
                    break;
                }
                if (statusId === statusInfo.worker_accepted_preorder.status_id) {
                    if (isTaxiOrder) {
                        statusLabel = statusLabelTranslation(req, statusGroup, statusId, positionId);
                    } else {
                        statusLabel = req.i18n.__('Courier has accepted an order');
                    }
                    command_params = {
                        'order_id': orderId.toString(),
                        'status': statusGroup.toString(),
                        'status_label': statusLabel.toString(),
                        'car': carDescr.toString(),
                        'worker': worker.toString(),
                        'tel': workerPhone.toString()
                    };
                    break;
                }
                if (statusId === statusInfo.worker_refused_preorder.status_id) {
                    if (isTaxiOrder) {
                        statusLabel = statusLabelTranslation(req, statusGroup, statusId, positionId);
                    } else {
                        statusLabel = req.i18n.__('Courier order refusing');
                    }
                    command_params = {
                        'order_id': orderId.toString(),
                        'status': statusGroup.toString(),
                        'status_label': statusLabel.toString()
                    };
                    break;
                }
                break;
            case "car_assigned":
                if (isTaxiOrder) {
                    statusLabel = statusLabelTranslation(req, statusGroup, statusId, positionId);
                } else {
                    statusLabel = req.i18n.__('The courier left');
                }
                command_params = {
                    'order_id': orderId.toString(),
                    'status': statusGroup.toString(),
                    'status_label': statusLabel.toString(),
                    'car': carDescr.toString(),
                    'worker': worker.toString(),
                    'car_time': time.toString(),
                    'tel': workerPhone.toString()
                };
                sound = "sound_beep.aiff";
                break;
            case "car_at_place":
                if (isTaxiOrder) {
                    statusLabel = statusLabelTranslation(req, statusGroup, statusId, positionId);
                } else {
                    statusLabel = req.i18n.__('The courier arrived');
                }
                command_params = {
                    'order_id': orderId.toString(),
                    'status': statusGroup.toString(),
                    'status_label': statusLabel.toString(),
                    'car': carDescr.toString(),
                    'worker': worker.toString(),
                    'tel': workerPhone.toString()
                };
                sound = "sound_beep.aiff";
                break;
            case "executing":
                if (isTaxiOrder) {
                    statusLabel = statusLabelTranslation(req, statusGroup, statusId, positionId);
                } else {
                    statusLabel = req.i18n.__('Execution of an order');
                }
                command_params = {
                    'order_id': orderId.toString(),
                    'status': statusGroup.toString(),
                    'status_label': statusLabel.toString(),
                    'car': carDescr.toString(),
                    'worker': worker.toString(),
                    'tel': workerPhone.toString()
                };
                break;
            case "completed":
                statusLabel = statusLabelTranslation(req, statusGroup, statusId, positionId);
                command_params = {
                    'order_id': orderId.toString(),
                    'status': statusGroup.toString(),
                    'status_label': statusLabel.toString(),
                    'cost': (price === null) ? "0" : price.toString(),
                    'cost_currency': currency.toString()
                };
                break;
            case "rejected":
                statusLabel = statusLabelTranslation(req, statusGroup, statusId, positionId);
                command_params = {
                    'order_id': orderId.toString(),
                    'status': statusGroup.toString(),
                    'status_label': statusLabel.toString()
                };
                break;
            default:
                command_params = {
                    'order_id': orderId.toString(),
                    'status': statusGroup.toString(),
                    'status_label': statusLabel.toString()
                };
                break;
        }
        let contentAvailable = null;
        let actionLocKey = null;
        let category = null;
        let clientDeviceToken = deviceToken;
        let clientDevice = typeDevice;
        if (message === null) {
            sound = null;
        }
        const settings = {
            title: title,
            message: message,
            contentAvailable: contentAvailable,
            actionLocKey: actionLocKey,
            category: category,
            icon: null,
            sound: sound,
            command: command,
            command_params: command_params,
        };
        if (typeof clientDevice === "string" && clientDevice.length > 0 && typeof clientDeviceToken === "string" && clientDeviceToken.length > 0) {
            clientDevice = clientDevice.toLowerCase();
            orderLogger(orderId, 'info', `tenantId:${tenantId} clientDevice:${clientDevice} clientDeviceToken:${clientDeviceToken} clientAppId:${clientAppId} settings:${JSON.stringify(settings)}`);
            pusher.sendPush(tenantId, clientDevice, clientDeviceToken, "client", clientAppId, settings)
                .then(result => {
                    serviceLogger('info', `${clientDevice} push result: ${JSON.stringify(result)}`);
                    orderLogger(orderId, 'info', `${clientDevice} push result: ${JSON.stringify(result)}`);
                }).catch(err => {
                serviceLogger('error', `${clientDevice} push error: ${JSON.stringify(err.message || err)}`);
                orderLogger(orderId, 'error', `${clientDevice} push error: ${JSON.stringify(err.message || err)}`);
            });
        }
        else {
            serviceLogger('error', `BAD typeDevice or deviceToken. typeDevice:${clientDevice}; deviceToken:${clientDeviceToken}`);
            orderLogger(orderId, 'error', `BAD typeDevice or deviceToken. typeDevice:${clientDevice}; deviceToken:${clientDeviceToken}`);
        }

    });
};

/**
 * is order processed through the external exchange
 * @param activeOrder
 * @return {boolean}
 */
function isOrderProcessedThroughTheExternalExchange(activeOrder) {
    if (activeOrder && activeOrder['exchange_info'] && activeOrder['exchange_info']['order']) {
        return true;
    }
}


/**
 * Get car info from external exchange
 * @param activeOrder
 * @return {*}
 */
function getCarInfoFromExternalExchange(activeOrder) {
    try {
        const car = activeOrder['exchange_info']['order']['car'];
        return {
            brand: car['name'],
            model: '',
            color: car['color'],
            number: car['gos_number']
        }
    } catch (err) {
        return null;
    }


}


