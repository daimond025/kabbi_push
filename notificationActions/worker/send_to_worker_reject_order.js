"use strict";

const serviceLogger = require('../../services/logger').serviceLogger;
const pusher = require('../../services/pusher/pushService');
const redisWorkerManager = require('../../dbs/redisWorkerManager');
const sqlManager = require('../../dbs/sqlManager');
const async = require('async');


/**
 * Send Push to worker on reject his order
 * TYPE REQUEST: GET
 * @param  req
 * @param  res
 * req.body:
 *   order_id
 *   worker_callsign
 *   tenant_id
 *   status_id
 */
module.exports = function controller(req, res) {
    serviceLogger('info', "Called method: send_to_worker_reject_order");
    serviceLogger('info', req.query);
    const answer = '{"result":1}';
    res.write(answer);
    res.end();
    const orderId = req.query.order_id;
    const workerCallsign = req.query.worker_callsign;
    const tenantId = parseInt(req.query.tenant_id);
    const statusId = req.query.status_id;
    const contentAvailable = null;
    const actionLocKey = null;
    const category = null;
    const sound = 'default';
    const command = "order_is_rejected";
    const command_params = {'order_id': orderId, 'status_id': statusId};
    redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
        if (err) {
            serviceLogger('error', err);
        } else {
            if (workerData && workerData.worker && workerData.worker.device && workerData.worker.device_token) {
                let workerDevice = workerData.worker.device;
                workerDevice = workerDevice.toLowerCase();
                const workerDeviceToken = workerData.worker.device_token;
                if (typeof workerDevice === "string" && workerDevice.length > 0 && typeof workerDeviceToken === "string" && workerDeviceToken.length > 0) {
                    const lang = workerData.worker.lang;
                    req.i18n.setLocale(lang);
                    const OrderPhrase = req.i18n.__('Order');
                    const RejectPhrase = req.i18n.__('Rejected');
                    const title = OrderPhrase + " №" + orderId + " " + RejectPhrase;
                    const message = OrderPhrase + " №" + orderId + " " + RejectPhrase;
                    const settings = {
                        title: title,
                        message: message,
                        contentAvailable: contentAvailable,
                        actionLocKey: actionLocKey,
                        category: category,
                        icon: null,
                        sound: sound,
                        command: command,
                        command_params: command_params,
                    };
                    pusher.sendPush(tenantId, workerDevice, workerDeviceToken, "worker",null, settings)
                        .then(result => {
                            serviceLogger('info', `${workerDevice} push result: ${JSON.stringify(result)}`);
                        }).catch(err => {
                        serviceLogger('error', `${workerDevice} push error: ${JSON.stringify(err)}`);
                    });
                } else {
                    serviceLogger('error', `Bad worker params`);
                }
            } else {
                serviceLogger('error', `Bad worker params`);
            }
        }
    })
};



