'use strict';

const serviceLogger = require('../../services/logger').serviceLogger;
const pusher = require('../../services/pusher/pushService');
const redisWorkerManager = require('../../dbs/redisWorkerManager');
const redisOrderManager = require('../../dbs/redisOrderManager');
const sqlManager = require('../../dbs/sqlManager');
const moment = require('moment');


/**
 * Send push message to worker that he is assigned at order
 * TYPE REQUEST: GET
 * @param  req
 *  req.body:
 *   tenant_id
 *   order_id
 *   worker_callsign
 *   is_pre_order
 * @param res
 */
module.exports = function controller(req, res) {
    serviceLogger('info', "Called method: send_assign_at_order");
    serviceLogger('info', req.query);
    const answer = '{"result":1}';
    res.write(answer);
    res.end();
    const orderId = req.query.order_id;
    const workerCallsign = req.query.worker_callsign;
    const tenantId = req.query.tenant_id;
    const isPreOrder = req.query.is_pre_order;
    let command = "assign_at_order";
    if (isPreOrder) {
        command = "assign_at_pre_order";
    }
    const assignPhrase = req.i18n.__(`You are assigned to the order. Car delivery time`);
    let orderDateString = '';
    const command_params = {'order_id': orderId};

    redisOrderManager.getOrder(tenantId, orderId, (err, order) => {
        if (err) {
            serviceLogger('error', err);
        } else {
            redisWorkerManager.getWorker(tenantId, workerCallsign, (err, worker) => {
                if (err) {
                    serviceLogger('info', "Worker is not on shift. Get data form sqlDb");
                    sqlManager.getWorker(tenantId, workerCallsign, (err, worker) => {
                        if (err) {
                            serviceLogger('error', err);
                        } else {
                            let workerDevice = worker.device;
                            let workerDeviceToken = worker.device_token;
                            let lang = worker.lang;
                            req.i18n.setLocale(lang);
                            moment.locale(lang);
                            if (order && order.order_time) {
                                let orderTime = parseInt(order.order_time);
                                orderDateString = moment.unix(orderTime).format("LLL");
                            }
                            let title = `${assignPhrase}: ${orderDateString}`;
                            let message = `${assignPhrase}: ${orderDateString}`;
                            const settings = {
                                title: title,
                                message: message,
                                contentAvailable: null,
                                actionLocKey: null,
                                category: null,
                                icon: null,
                                sound: 'default',
                                command: command,
                                command_params: command_params
                            };
                            sendMessage(tenantId, workerDevice, workerDeviceToken, settings);
                        }
                    })
                } else {
                    let workerDevice = worker.worker.device;
                    const workerDeviceToken = worker.worker.device_token;
                    const lang = worker.worker.lang;
                    req.i18n.setLocale(lang);
                    moment.locale(lang);
                    if (order && order.order_time) {
                        let orderTime = parseInt(order.order_time);
                        orderDateString = moment.unix(orderTime).format("LLL");
                    }
                    let title = `${assignPhrase}: ${orderDateString}`;
                    let message = `${assignPhrase}: ${orderDateString}`;
                    const settings = {
                        title: title,
                        message: message,
                        contentAvailable: null,
                        actionLocKey: null,
                        category: null,
                        icon: null,
                        sound: 'default',
                        command: command,
                        command_params: command_params,

                    };
                    sendMessage(tenantId, workerDevice, workerDeviceToken, settings)
                        .then(result => {
                            serviceLogger('info', `${workerDevice} push result: ${JSON.stringify(result)}`);
                        }).catch(err => {
                        serviceLogger('error', `${workerDevice} push error: ${JSON.stringify(err)}`);
                    });
                }
            });
        }
    });
    function sendMessage(tenantId, workerDevice, workerDeviceToken, settings) {
        if (typeof workerDevice === "string" && workerDevice.length > 0 && typeof workerDeviceToken === "string" && workerDeviceToken.length > 0) {
            workerDevice = workerDevice.toLowerCase();
            pusher.sendPush(tenantId, workerDevice, workerDeviceToken, "worker",null, settings)
                .then(result => {
                    serviceLogger('info', `${workerDevice} push result: ${JSON.stringify(result)}`);
                }).catch(err => {
                serviceLogger('error', `${workerDevice} push error: ${JSON.stringify(err)}`);
            });
        } else {
            serviceLogger('error', `Bad workerDevice or workerDeviceToken. typeDevice:${workerDevice}; deviceToken:${workerDeviceToken}`);
        }
    }
};
