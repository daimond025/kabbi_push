'use strict';

const serviceLogger = require('../../services/logger').serviceLogger;
const pusher = require('../../services/pusher/pushService');

/**
 * destroy IosDriverAppSenderService
 * Using when updates ios certs for driver app
 */
module.exports = function controller(req, res) {
    serviceLogger('info', "Called method: destroy_ios_driver_app_sender_service");
    serviceLogger('info', req.query);
    const result = pusher.destroyIosWorkerAppSenderService();
    process.send({
        cmd: 'destroy_ios_driver_app_sender_service',
    });
    let answer = '{"result":0}';
    if (result === true) {
        answer = '{"result":1}';
        res.write(answer);
        res.end();
    } else {
        res.write(answer);
        res.end();
    }
};
