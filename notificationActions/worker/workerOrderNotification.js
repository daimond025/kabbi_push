'use order';


const serviceLogger = require('../../services/logger').serviceLogger;
const orderLogger = require('../../services/logger').orderLogger;
const pusher = require('../../services/pusher/pushService');
const redisWorkerManager = require('../../dbs/redisWorkerManager');
const redisOrderManager = require('../../dbs/redisOrderManager');
const async = require('async');
const sqlManager = require('../../dbs/sqlManager');
const myCache = require('../../dbs/cache');
const templateService = require('../../services/templateService');
const PHPUnserialize = require('php-unserialize');
const moment = require('moment');
const currencyService = require('../../services/currencyService');
const config = require("../../services/lib").getConfig();

/**
 * Notify worker about new order
 * @param {number} tenantId
 * @param {number} orderId
 * @param {boolean} soundOn
 * @param {boolean} forAll
 * @param {boolean} orderIsNew
 * @param {array} workers
 * @return {Promise<any>}
 */
function notifyAboutNewOrder(tenantId, orderId, soundOn, forAll, orderIsNew, workers) {
    return new Promise((resolve, reject) => {
        let sound = null;
        if (soundOn) {
            sound = "new_free.aiff";
        }
        let command = "update_pre_orders";
        if (orderIsNew) {
            command = "update_free_orders"
        }
        if (!Array.isArray(workers) && workers.length > 0) {
            serviceLogger('error', `notifyAboutNewOrder-> workers: empty array`);
            return resolve(true);
        }
        let settings;
        let getMessageFormCache = true;
        if (config.MY_ENV) {
            getMessageFormCache = false;
        }
        getMessageText(tenantId, orderId, orderIsNew, getMessageFormCache)
            .then(text => {
                settings = {
                    title: text,
                    message: text,
                    contentAvailable: null,
                    actionLocKey: null,
                    category: null,
                    icon: null,
                    sound: sound,
                    command: command,
                    command_params: {},

                };
                return getWorkerDevices(tenantId, workers, forAll);
            })
            .catch(err => {
                serviceLogger('error', `notifyAboutNewOrder->getMessageText error: ${err.message}`);
                return reject(err);
            })
            .then(workerDevices => {
                if (workerDevices.ios.length > 0) {
                    pusher.sendPush(tenantId, 'ios', workerDevices.ios, "worker", null, settings)
                        .then(result => {
                            serviceLogger('info', `notifyAboutNewOrder->ios push result: ${JSON.stringify(result)}`);
                            orderLogger(orderId, 'info', `notifyAboutNewOrder->pusher.sendPush->ios sender result:${result}. push: ${JSON.stringify(settings)}.`);
                        })
                        .catch(err => {
                            serviceLogger('error', `notifyAboutNewOrder->ios push error: ${JSON.stringify(err)}`);
                            orderLogger(orderId, 'error', `notifyAboutNewOrder->pusher.sendPush->ios sender error:${err.message}. push: ${JSON.stringify(settings)}.`);
                        });
                }
                if (workerDevices.android.length > 0) {
                    pusher.sendPush(tenantId, 'android', workerDevices.android, "worker", null, settings)
                        .then(result => {
                            serviceLogger('info', `notifyAboutNewOrder->android push result: ${JSON.stringify(result)}`);
                            orderLogger(orderId, 'info', `notifyAboutNewOrder->pusher.sendPush->android sender result:${result}. push: ${JSON.stringify(settings)}.`);
                        })
                        .catch(err => {
                            serviceLogger('error', `notifyAboutNewOrder->android push error: ${JSON.stringify(err)}`);
                            orderLogger(orderId, 'error', `notifyAboutNewOrder->pusher.sendPush->android sender error:${err.message}. push: ${JSON.stringify(settings)}.`);
                        });
                }
                return resolve(true);
            })
            .catch(err => {
                serviceLogger('error', `notifyAboutNewOrder->getWorkerDevices error: ${err}`);
                return reject(err);
            });
    })
}

/**
 * Get message text
 * @param {number} tenantId
 * @param {number} orderId
 * @param {boolean} orderIsNew
 * @param {boolean} fromCache
 * @returns {Promise<any>}
 */
function getMessageText(tenantId, orderId, orderIsNew, fromCache) {
    return new Promise((resolve, reject) => {
        redisOrderManager.getOrder(tenantId, orderId, (err, order) => {
            if (err) {
                return reject(err)
            }
            const positionId = order.position_id;
            const cityId = order.city_id;
            let orderType = 1;
            if (!orderIsNew) {
                orderType = 6;
            }
            let messageTemplate;
            if (!fromCache) {
                let messageTemplate;
                sqlManager.getWorkerPushTemplate({
                    tenantId: tenantId,
                    cityId: cityId,
                    positionId: positionId,
                    type: orderType
                })
                    .then(template => {
                        messageTemplate = template;
                        return getTemplateMessageParams(order)
                    })
                    .then(messageParams => {
                        return resolve(templateService.replace(messageTemplate, messageParams));
                    })
                    .catch(err => {
                        return reject(err);
                    })
            } else {
                const cacheKey = `worker_order_template_text_${tenantId}_${cityId}_${positionId}_${orderType}`;
                myCache.get(cacheKey, (err, templateText) => {
                    if (err) {
                        return reject(err);
                    }
                    if (templateText) {
                        return resolve(templateText)
                    }
                    sqlManager.getWorkerPushTemplate({
                        tenantId: tenantId,
                        cityId: cityId,
                        positionId: positionId,
                        type: orderType
                    })
                        .then(template => {
                            messageTemplate = template;
                            return getTemplateMessageParams(order)
                        })
                        .then(messageParams => {
                            let templateText = templateService.replace(messageTemplate, messageParams);
                            myCache.set(cacheKey, templateText, (err, result) => {
                                if (err) {
                                    serviceLogger('error', `notifyAboutNewOrder->error of template text to cache: ${cacheKey}`);
                                }
                                return resolve(templateText);
                            });
                        })
                        .catch(err => {
                            return reject(err);
                        })
                })
            }
        })
    });
}

/**
 * Get template message params
 * @param order
 * @return {Promise<{address: string, date: string, time: string, tariff: string, price: string, currency: string}>}
 */
function getTemplateMessageParams(order) {
    return new Promise((resolve, reject) => {
        let params = {
            address: '',
            date: '',
            time: '',
            tariff: '',
            price: '',
            currency: '',
        };
        try {
            const addressData = PHPUnserialize.unserialize(order.address);
            const addressFrom = addressData.A;
            const cityFrom = addressFrom.city ? addressFrom.city : '';
            const streetFrom = addressFrom.street ? addressFrom.street : '';
            const houseFrom = addressFrom.house ? addressFrom.house : '';
            params.address = cityFrom + ' ' + streetFrom + ' ' + houseFrom;
        } catch (err) {
            serviceLogger('error', `notifyAboutNewOrder->error of parsing address: ${err.message}`);
        }

        try {
            params.tariff = order.tariff.name;
        } catch (err) {
            serviceLogger('error', `notifyAboutNewOrder->-error of parsing tariff name: ${err.message}`);
        }

        try {
            params.price = order.predv_price;
        } catch (err) {
            serviceLogger('error', `notifyAboutNewOrder->->error of parsing predv_price: ${err.message}`);
        }

        try {
            let orderTime = parseInt(order.order_time);
            params.date = moment.unix(orderTime).format("HH:mm:ss YYYY-MM-DD");
            params.time = moment.unix(orderTime).format('HH:mm:ss');
        } catch (err) {
            serviceLogger('error', `notifyAboutNewOrder->->error of parsing order_time: ${err.message}`);
        }

        let currencyId;
        try {
            currencyId = order.currency_id;
        } catch (err) {
            serviceLogger('error', `notifyAboutNewOrder->->error of parsing currency_id: ${err.message}`);
        }

        if (!currencyId) {
            return resolve(params);
        }
        currencyService.getCode(currencyId)
            .then(currencyCode => {
                params.currency = currencyCode;
                return resolve(params);
            })
            .catch(err => {
                serviceLogger('error', `notifyAboutNewOrder->->currencyService.getCode error: ${err.message}`);
                return resolve(params);
            })
    });
}

/**
 * Get worker devices
 * @param {number} tenantId
 * @param {array} workersCallsign
 * @param {boolean} isForAllWorkers 1/0
 * @returns {Promise<object>}
 */
function getWorkerDevices(tenantId, workersCallsign, isForAllWorkers) {
    return new Promise((resolve, reject) => {
        if (!isForAllWorkers) {
            getOnlineWorkerDevices(tenantId, workersCallsign, (err, workerDevices) => {
                if (err) {
                    return reject(err);
                }
                return resolve(workerDevices)

            })
        } else {
            getAllListedWorkerDevices(tenantId, workersCallsign, (err, workerDevices) => {
                if (err) {
                    return reject(err);
                }
                return resolve(workerDevices)
            })
        }
    });
}

/**
 * Get online worker devices
 * @param tenantId
 * @param workersCallsign
 * @param resultCb
 */
function getOnlineWorkerDevices(tenantId, workersCallsign, resultCb) {
    const workerDevices = {
        'android': [],
        'ios': []
    };
    async.each(workersCallsign, (workerCallsign, callback) => {
        redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
            if (err) {
                serviceLogger('error', `Error of getWorker tenantId: ${tenantId} workerCallsign: ${workerCallsign}`);
                return callback(err);
            } else {
                if (workerData && workerData.worker && workerData.worker.device && workerData.worker.device_token) {
                    const workerDevice = workerData.worker.device;
                    const workerDeviceToken = workerData.worker.device_token;
                    if (typeof workerDevice === "string" && workerDevice.length > 0 && typeof workerDeviceToken === "string" && workerDeviceToken.length > 0) {
                        if (workerDevice === "IOS") {
                            workerDevices.ios.push(workerDeviceToken)
                        } else if (workerDevice === "ANDROID") {
                            workerDevices.android.push(workerDeviceToken);
                        }
                    } else {
                        serviceLogger('error', `Empty workerDevice or workerDeviceToken`);
                    }
                } else {
                    serviceLogger('error', `Empty workerDevice or workerDeviceToken`);
                }
                callback(null, 1);
            }
        })
    }, (err) => {
        if (err) {
            serviceLogger('error', err);
        }
        return resultCb(null, workerDevices);
    });
}

/**
 * Get all listed worker devices
 * @param tenantId
 * @param workersCallsign
 * @param resultCb
 */
function getAllListedWorkerDevices(tenantId, workersCallsign, resultCb) {
    const workerDevices = {
        'android': [],
        'ios': []
    };
    sqlManager.getListedWorkers(tenantId, workersCallsign, (err, workers) => {
        if (err) {
            return resultCb(err);
        }
        if (!Array.isArray(workers)) {
            return resultCb(new Error('No workers in db'));
        }
        for (let worker of workers) {
            if (worker.device && worker.device_token) {
                let workerDevice = worker.device;
                let workerDeviceToken = worker.device_token;
                if (typeof workerDevice === "string" && workerDevice.length > 0 && typeof workerDeviceToken === "string" && workerDeviceToken.length > 0) {
                    if (workerDevice === "IOS") {
                        workerDevices.ios.push(workerDeviceToken)
                    } else if (workerDevice === "ANDROID") {
                        workerDevices.android.push(workerDeviceToken);
                    }
                }
            }
        }
        return resultCb(null, workerDevices)
    });
}


exports.notifyAboutNewOrder = notifyAboutNewOrder;