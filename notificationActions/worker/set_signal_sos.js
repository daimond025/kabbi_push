"use strict";

const redisWorkersManager = require('../../dbs/redisWorkerManager');
const pusher = require('../../services/pusher/pushService');
const async = require('async');
const request = require('request');
const sqlManager = require("../../dbs/sqlManager");
const serviceLogger = require('../../services/logger').serviceLogger;
const config = require("../../services/lib").getConfig();
const geoservice_Api = config.SERVICE_GEO_URL;
const md5 = require("md5");
/**
 * Send sos signal to workers
 * TYPE REQUEST: GET
 * @param  req
 * req.body:
 *  tenant_id
 *  worker_callsign
 *  worker_lat
 *  worker_lon
 * @param res
 */
module.exports = function controller(req, res) {
    serviceLogger('info', "Called method: set_signal_sos");
    serviceLogger('info', req.query);
    const answer = '{"result":1}';
    res.write(answer);
    res.end();
    const tenantId = parseInt(req.query.tenant_id);
    const workerCallsign = req.query.worker_callsign;
    const workerLat = parseFloat(req.query.worker_lat);
    const workerLon = parseFloat(req.query.worker_lon);
    async.parallel([
        function getWorker(callback) {
            redisWorkersManager.getWorker(tenantId, workerCallsign, (err, workerItem) => {
                if (err) {
                    serviceLogger('error', err);
                    return callback(null, null);
                }
                return callback(null, workerItem);
            })
        },
        function getAllWorkers(callback) {
            redisWorkersManager.getAllWorkers(tenantId, (err, workers) => {
                if (err) {
                    serviceLogger('error', err);
                    return callback(null, null);
                }
                return callback(null, workers);
            });
        },
        function findAddressByCoords(callback) {
            sqlManager.getGeoServiceApiKey((err, geoServiceApiKey) => {
                if (err) {
                    serviceLogger('error', err);
                    return callback(null, null);
                }
                const queryString = `format=gootax&point.lat=${workerLat}&point.lon=${workerLon}&tenant_id=${tenantId}&type_app=frontend`;
                const hash = md5(queryString + geoServiceApiKey);
                const requestOptions = {
                    url: geoservice_Api + 'v1/reverse?' + queryString + `&hash=${hash}`
                };

                function sendRequest(error, response, body) {
                    if (!error && parseInt(response.statusCode) === 200) {
                        try {
                            const info = JSON.parse(body);
                            if (info.results && Array.isArray(info.results) && info.results[0]) {
                                return callback(null, info.results[0]);
                            }
                        } catch (err) {
                            serviceLogger('error', err);
                            return callback(err);
                        }
                    } else {
                        return callback(new Error('findAddressByCoords bad result'));
                    }
                }

                request(requestOptions, sendRequest);
            })

        }
    ], function (err, results) {
        if (err) {
            serviceLogger('error', err.message);
        } else {
            let workerItem, workersArr, addressData;
            if (results[0]) {
                workerItem = results[0];
            }
            if (results[1]) {
                workersArr = results[1];
            }
            if (results[2]) {
                addressData = results[2];
            }
            if (workerItem && workersArr && addressData) {
                const cityId = workerItem.worker.city_id;
                const workersIos = [];
                const workerAndroid = [];
                async.each(workersArr, (workerData, callback) => {
                    if (workerData && workerData.worker && workerData.worker.device
                        && workerData.worker.device_token
                        && parseInt(workerData.worker.city_id) === parseInt(cityId)
                    ) {
                        const workerDevice = workerData.worker.device;
                        const workerDeviceToken = workerData.worker.device_token;
                        if (typeof workerDevice === "string" && workerDevice.length > 0 && typeof workerDeviceToken === "string" && workerDeviceToken.length > 0) {
                            if (workerDevice === "IOS") {
                                workersIos.push(workerDeviceToken)
                            } else if (workerDevice === "ANDROID") {
                                workerAndroid.push(workerDeviceToken);
                            }
                        }
                    } else {
                        serviceLogger('error', `Bad worker params`);
                    }
                    return callback();
                }, (err) => {
                    if (err) {
                        serviceLogger('error', err);
                    }
                    let title = "SOS!!!";
                    let message = "SOS!!!";
                    let carName = "";
                    let carColor = "";
                    let carNumber = "";
                    let hasCar = parseInt(workerItem.position.has_car);
                    if (hasCar) {
                        carName = workerItem.car.name;
                        carNumber = workerItem.car.gos_number;
                        carColor = workerItem.car.color ? workerItem.car.color : carColor;
                        carColor = carColor.toLowerCase();
                        carColor = req.i18n.__(carColor);
                        message = carName + ' ' + carColor + ' ' + carNumber + ' SOS!!!';
                    } else {
                        message = 'SOS!!!';
                    }
                    let contentAvailable = null;
                    let actionLocKey = null;
                    let category = null;
                    let sound = "sos";
                    let command = "sos";
                    let fullAddress = null;
                    if (addressData) {
                        if (addressData.address) {
                            fullAddress = `${addressData.address.label}, ${addressData.address.city}`;
                        }
                    }
                    const command_params = {
                        'lat': workerLat,
                        'lon': workerLon,
                        'car_data': hasCar ? carName + ' ' + carColor + ' ' + carNumber : "",
                        'worker_callsign': workerCallsign,
                        'address': fullAddress
                    };

                    const settings = {
                        title: title,
                        message: message,
                        contentAvailable: contentAvailable,
                        actionLocKey: actionLocKey,
                        category: category,
                        icon: null,
                        sound: sound,
                        command: command,
                        command_params: command_params,
                    };

                    if (workersIos.length > 0) {
                        let workerDevice = 'ios';
                        pusher.sendPush(tenantId, workerDevice, workersIos, "worker", null, settings)
                            .then(result => {
                                serviceLogger('info', `${workerDevice} push result: ${JSON.stringify(result)}`);
                            }).catch(err => {
                            serviceLogger('error', `${workerDevice} push error: ${JSON.stringify(err)}`);
                        });
                    }
                    if (workerAndroid.length > 0) {
                        let workerDevice = 'android';
                        pusher.sendPush(tenantId, workerDevice, workerAndroid, "worker", null, settings)
                            .then(result => {
                                serviceLogger('info', `${workerDevice} push result: ${JSON.stringify(result)}`);
                            }).catch(err => {
                            serviceLogger('error', `${workerDevice} push error: ${JSON.stringify(err)}`);
                        });
                    }
                });

            } else {
                serviceLogger('info', "Dont need send SOS");
            }
        }
    });
};




