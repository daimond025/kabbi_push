'use strict';

const serviceLogger = require('../../services/logger').serviceLogger;
const pusher = require('../../services/pusher/pushService');
const redisWorkerManager = require('../../dbs/redisWorkerManager');
const sqlManager = require('../../dbs/sqlManager');
const async = require('async');
const cache = require('../../dbs/cache');

/**
 * Send personal push message to all workers in city (using by chat)
 * TYPE REQUEST: POST
 * @param req
 * req.body:
 *   tenant_login
 *   city_id
 *   message
 *   sender_f
 *   sender_i
 *   sender_o
 *   sender_id
 *   sender_type
 * @param res
 */
module.exports = function controller(req, res) {
    serviceLogger('info', "Called method: send_push_message_to_all_workers");
    serviceLogger('info', req.query);
    const answer = '{"result":1}';
    res.write(answer);
    res.end();
    const tenantLogin = req.body.tenant_login;
    const cityId = req.body.city_id;
    const message = req.body.message;
    const senderF = req.body.sender_f;
    const senderI = req.body.sender_i;
    const senderO = req.body.sender_o;
    const senderId = req.body.sender_id;
    const senderType = req.body.sender_type;
    const cacheKey = 'tenant_' + tenantLogin;
    cache.get(cacheKey, (err, tenantId) => {
        if (err) {
            serviceLogger('error', err);
        } else {
            if (tenantId) {
                sendPushToAllWorkers(tenantId, cityId, message, senderF, senderI, senderO, senderId, senderType)
            } else {
                sqlManager.getTenantIdbyDomain(tenantLogin, (err, tenantId) => {
                    if (err) {
                        serviceLogger('error', err);
                    } else {
                        cache.set(cacheKey, tenantId, function (err, success) {
                            if (!err && success) {
                                serviceLogger('info ', "Tenant_id stored in cache " + tenantId);
                            }
                        });
                        sendPushToAllWorkers(tenantId, cityId, message, senderF, senderI, senderO, senderId, senderType)
                    }
                })
            }
        }
    });

    /**
     * Send push to all workers in city
     * @param  tenantId
     * @param  cityId
     * @param  messageData
     * @param  senderF
     * @param  senderI
     * @param  senderO
     * @param  senderId
     * @param  senderType
     */
    function sendPushToAllWorkers(tenantId, cityId, messageData, senderF, senderI, senderO, senderId, senderType) {
        const workerBlackListCallsignArr = [];
        redisWorkerManager.getAllWorkers(tenantId, (err, workers) => {
            if (err) {
                serviceLogger('error', err);
            }
            if (!workers) {
                workers = []
            }
            async.each(workers, (workerData, callback) => {
                if (workerData && workerData.worker && workerData.worker.city_id) {
                    if (parseInt(workerData.worker.city_id) === parseInt(cityId)) {
                        workerBlackListCallsignArr.push(workerData.worker.callsign);
                    }
                }
                return callback();
            }, (err) => {
                if (err) {
                    serviceLogger('error', err);
                }
                sqlManager.getNotListedWorkersInCity(tenantId, cityId, workerBlackListCallsignArr, (err, workers) => {
                    if (err) {
                        serviceLogger('error', err);
                    } else {
                        const senderI1 = senderI.charAt(0);
                        const senderO1 = senderO.charAt(0);
                        const title = senderF + ' ' + senderI1 + ' ' + senderO1 + ': ' + messageData;
                        const message = senderF + ' ' + senderI1 + ' ' + senderO1 + ': ' + messageData;
                        const sound = "message_sound.wav";
                        const command = "new_main_message";
                        const command_params = {};
                        const workersIos = [];
                        const workerAndroid = [];
                        const settings = {
                            title: title,
                            message: message,
                            contentAvailable: null,
                            actionLocKey: null,
                            category: null,
                            icon: null,
                            sound: sound,
                            command: 'new_main_message',
                            command_params: command_params,

                        };
                        async.each(workers, (workerData, callback) => {
                            if (workerData && workerData.device && workerData.device_token) {
                                if (senderType === "worker" && parseInt(senderId) === parseInt(workerData.callsign)) {
                                    serviceLogger('info', `worker ${workerData.callsign} is message owner. Dont need send notify`);
                                } else {
                                    const workerDevice = workerData.device;
                                    const workerDeviceToken = workerData.device_token;
                                    if (typeof workerDevice === "string" && workerDevice.length > 0 && typeof workerDeviceToken === "string" && workerDeviceToken.length > 0) {
                                        if (workerDevice === "IOS") {
                                            workersIos.push(workerDeviceToken)
                                        } else if (workerDevice === "ANDROID") {
                                            workerAndroid.push(workerDeviceToken);
                                        }
                                    }
                                }
                            } else {
                                serviceLogger('info', `Bad worker params`);
                                serviceLogger('info', workerData);
                            }
                            return callback();
                        }, function (err) {
                            if (err) {
                                serviceLogger('error', err);
                            }
                            if (workersIos.length > 0) {
                                pusher.sendPush(tenantId, 'ios', workersIos, "worker", null, settings).then(result => {
                                    serviceLogger('info', `ios push result: ${JSON.stringify(result)}`);
                                }).catch(err => {
                                    serviceLogger('error', `ios push error: ${JSON.stringify(err)}`);
                                });
                            }
                            if (workerAndroid.length > 0) {
                                pusher.sendPush(tenantId, 'android', workerAndroid, "worker", null, settings).then(result => {
                                    serviceLogger('info', `android push result: ${JSON.stringify(result)}`);
                                }).catch(err => {
                                    serviceLogger('error', `android push error: ${JSON.stringify(err)}`);
                                });
                            }
                        });
                    }
                });

            })
        });


    }
};
