'use strict';

const serviceLogger = require('../../services/logger').serviceLogger;
const pusher = require('../../services/pusher/pushService');

/**
 * destroy AndroidDriverAppSenderService
 * Using when updates gcm key  for driver app
 */
module.exports = function controller(req, res) {
    serviceLogger('info', "Called method: destroy_android_driver_app_sender_service");
    serviceLogger('info', req.query);
    const tenantId = req.query.tenant_id;
    const result = pusher.destroyAndroidWorkerAppSenderService(tenantId);
    process.send({
        cmd: 'destroy_android_driver_app_sender_service',
        tenant_id: tenantId
    });
    let answer = '{"result":0}';
    if (result === true) {
        answer = '{"result":1}';
        res.write(answer);
        res.end();
    } else {
        res.write(answer);
        res.end();
    }
}

