"use strict";

const serviceLogger = require('../../services/logger').serviceLogger;
const pusher = require('../../services/pusher/pushService');
const redisWorkerManager = require('../../dbs/redisWorkerManager');
const sqlManager = require('../../dbs/sqlManager');
const async = require('async');
const cache = require('../../dbs/cache');

/**
 * Send personal push message to worker (using by chat)
 * Will be sended if worker is NOT on shift
 * TYPE REQUEST: POST
 * @param req
 * req.body:
 *   tenant_login
 *   worker_callsign
 *   message
 *   sender_f
 *   sender_i
 *   sender_o
 * @param res
 */
module.exports = function controller(req, res) {
    serviceLogger('info', "Called method: send_push_message_to_worker");
    serviceLogger('info', req.body);
    const answer = '{"result":1}';
    res.write(answer);
    res.end();
    const tenantLogin = req.body.tenant_login;
    const callsign = req.body.worker_callsign;
    const message = req.body.message;
    const senderF = req.body.sender_f;
    const senderI = req.body.sender_i;
    const senderO = req.body.sender_o;
    const cacheKey = 'tenant_' + tenantLogin;
    cache.get(cacheKey, (err, tenantId) => {
        if (err) {
            serviceLogger('error', err);
        } else {
            if (tenantId) {
                sendPushToWorker(tenantId, callsign, message, senderF, senderI, senderO);
            } else {
                sqlManager.getTenantIdbyDomain(tenantLogin, (err, tenantId) => {
                    if (err) {
                        serviceLogger('error', err);
                    } else {
                        cache.set(cacheKey, tenantId, function (err, success) {
                            if (!err && success) {
                                serviceLogger('info ', "Tenant_id stored in cache " + tenantId);
                            }
                        });
                        sendPushToWorker(tenantId, callsign, message, senderF, senderI, senderO);
                    }
                })
            }
        }
    });

    function sendPushToWorker(tenantId, callsign, messageData, senderF, senderI, senderO) {
        redisWorkerManager.getWorker(tenantId, callsign, (err, workerData) => {
            if (err) {
                sqlManager.getWorker(tenantId, callsign, (err, workerData) => {
                    if (err || !workerData) {
                        serviceLogger('error', err);
                    } else {
                        if (workerData && workerData.device && workerData.device_token) {
                            let workerDevice = workerData.device.toLowerCase();
                            const workerDeviceToken = workerData.device_token;
                            const senderI1 = senderI.charAt(0);
                            const senderO1 = senderO.charAt(0);
                            const title = senderF + ' ' + senderI1 + ' ' + senderO1 + ': ' + messageData;
                            const message = senderF + ' ' + senderI1 + ' ' + senderO1 + ': ' + messageData;
                            const sound = "message_sound.wav";
                            const command = "new_message";
                            const command_params = {};
                            const settings = {
                                title: title,
                                message: message,
                                contentAvailable: null,
                                actionLocKey: null,
                                category: null,
                                icon: null,
                                sound: sound,
                                command: command,
                                command_params: command_params,
                            };
                            if (typeof workerDevice === "string" && workerDevice.length > 0 && typeof workerDeviceToken === "string" && workerDeviceToken.length > 0) {
                                pusher.sendPush(tenantId, workerDevice, workerDeviceToken, "worker",null, settings).then(result => {
                                    serviceLogger('info', `${workerDevice} push result: ${JSON.stringify(result)}`);
                                }).catch(err => {
                                    serviceLogger('error', `${workerDevice} push error: ${JSON.stringify(err)}`);
                                });
                            } else {
                                serviceLogger('info', `Bad worker params`);
                                serviceLogger('info', workerData);
                            }
                        }
                    }
                });
            } else {
                serviceLogger('info', 'Worker is on shift. Dont need send push notification');
            }
        });
    }
}
