"use strict";

const serviceLogger = require('../../services/logger').serviceLogger;
const pusher = require('../../services/pusher/pushService');
const redisWorkerManager = require('../../dbs/redisWorkerManager');
const sqlManager = require('../../dbs/sqlManager');
const async = require('async');


/**
 * Send Push to worker on new order (offer order for him)
 * TYPE REQUEST: GET
 * @param  req
 * @param  res
 * req.body:
 *   order_id
 *   worker_callsign
 *   tenant_id
 */
module.exports = function controller(req, res) {
    serviceLogger('info', "Called method: send_to_worker_new_order");
    serviceLogger('info', req.query);
    const answer = '{"result":1}';
    res.write(answer);
    res.end();
    const orderId = req.query.order_id;
    const workerCallsign = req.query.worker_callsign;
    const tenantId = parseInt(req.query.tenant_id);
    const contentAvailable = 1;
    const actionLocKey = 'SLIDE_LEFT';
    const category = 'OFFER_ORDER_CATEGORY';
    const sound = 'new_order.wav';
    const command = "get_order_for_id";
    const nowTimestamp = parseInt((new Date()).getTime() / 1000);
    const command_params = {
        'order_id': orderId,
        'timestamp': nowTimestamp
    };

    redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
        if (err) {
            serviceLogger('error', err);
        } else {
            if (workerData && workerData.worker && workerData.worker.device && workerData.worker.device_token) {
                let workerDevice = workerData.worker.device;
                workerDevice = workerDevice.toLowerCase();
                const workerDeviceToken = workerData.worker.device_token;
                if (typeof workerDevice === "string" && workerDevice.length > 0 && typeof workerDeviceToken === "string" && workerDeviceToken.length > 0) {
                    const lang = workerData.worker.lang;
                    req.i18n.setLocale(lang);
                    const NewOrderPhrase = req.i18n.__('New Order');
                    const title = NewOrderPhrase + " №" + orderId;
                    const message = NewOrderPhrase + " №" + orderId;
                    const settings = {
                        title: title,
                        message: message,
                        contentAvailable: contentAvailable,
                        actionLocKey: actionLocKey,
                        category: category,
                        icon: null,
                        sound: sound,
                        command: command,
                        command_params: command_params,
                    };
                    pusher.sendPush(tenantId, workerDevice, workerDeviceToken, "worker",null, settings)
                        .then(result => {
                            serviceLogger('info', `${workerDevice} push result: ${JSON.stringify(result)}`);
                        }).catch(err => {
                        serviceLogger('error', `${workerDevice} push error: ${JSON.stringify(err)}`);
                    });
                } else {
                    serviceLogger('error', `Bad worker params`);
                }
            } else {
                serviceLogger('error', `Bad worker params`);
            }
        }
    })
};

