'use strict';

const serviceLogger = require('../../services/logger').serviceLogger;
const workerOrderNotification = require('./workerOrderNotification');

/**
 * Send push message to update free orders list for workers
 * TYPE REQUEST: GET
 * @param  req
 *  req.body:
 *   tenant_id
 *   order_id
 *   sound_on
 *   workers
 * @param res
 */
module.exports = function controller(req, res) {
    serviceLogger('info', "Called method: send_free_order_for_all");
    serviceLogger('info', req.query);
    const answer = '{"result":1}';
    res.write(answer);
    res.end();
    const tenantId = parseInt(req.query.tenant_id);
    const orderId = parseInt(req.query.order_id);
    const soundOn = parseInt(req.query.sound_on) === 1;
    const forAll = parseInt(req.query.for_all) === 1;
    const workers = req.query.workers.split(',');
    workerOrderNotification.notifyAboutNewOrder(tenantId, orderId, soundOn, forAll, true, workers)
        .then(result => {
            serviceLogger('info', `send_free_order_for_all->result=${result}`);
        })
        .catch(err => {
            serviceLogger('error', `send_free_order_for_all->error=${err.message}`);
        })
};




