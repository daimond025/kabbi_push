"use strict";

const serviceLogger = require('../../services/logger').serviceLogger;
const pusher = require('../../services/pusher/pushService');
const redisWorkerManager = require('../../dbs/redisWorkerManager');
const sqlManager = require('../../dbs/sqlManager');
const async = require('async');


/**
 * Send Push to worker to remind his preorder
 * TYPE REQUEST: GET
 * @param  req
 * req.body:
 *   order_id
 *   order_number
 *   worker_callsign
 *   tenant_id
 *   remind_time
 * @param  res
 */
module.exports = function controller(req, res) {
    serviceLogger('info', "Called method: send_to_worker_remind_pre_order");
    serviceLogger('info', req.query);
    const answer = '{"result":1}';
    res.write(answer);
    res.end();
    const orderId = req.query.order_id;
    const orderNumber = req.query.order_number;
    const workerCallsign = req.query.worker_callsign;
    const tenantId = parseInt(req.query.tenant_id);
    const remindTime = req.query.remind_time;
    const contentAvailable = null;
    const actionLocKey = null;
    const category = null;
    const sound = 'default';
    const command = "remind_pre_order";
    const command_params = {'order_id': orderId};
    getWorkerDeviceData(tenantId, workerCallsign, (err, workerData) => {
        if (err) {
            serviceLogger('error', err);
        } else {
            const lang = workerData.lang;
            const workerDevice = workerData.device;
            const workerDeviceToken = workerData.device_token;
            if (typeof workerDevice === "string" && workerDevice.length > 0 && typeof workerDeviceToken === "string" && workerDeviceToken.length > 0) {
                req.i18n.setLocale(lang);
                const NotifyPhrase = req.i18n.__('Notification Order');
                const ReminePhrase = req.i18n.__('Before giving of a car remained');
                const MinutesPhrase = req.i18n.__('Minutes');
                const title = NotifyPhrase + " №" + orderNumber;
                const message = ReminePhrase + " " + remindTime + " " + MinutesPhrase;
                const settings = {
                    title: title,
                    message: message,
                    contentAvailable: contentAvailable,
                    actionLocKey: actionLocKey,
                    category: category,
                    icon: null,
                    sound: sound,
                    command: command,
                    command_params: command_params,
                };
                pusher.sendPush(tenantId, workerDevice, workerDeviceToken, "worker", null, settings)
                    .then(result => {
                        serviceLogger('info', `${workerDevice} push result: ${JSON.stringify(result)}`);
                    }).catch(err => {
                    serviceLogger('error', `${workerDevice} push error: ${JSON.stringify(err)}`);
                });
            } else {
                serviceLogger('error', `Bad worker params`);
            }
        }
    });
};


/**
 * Get worker device info
 * @param tenantId
 * @param workerCallsign
 * @param {function} callback
 */
function getWorkerDeviceData(tenantId, workerCallsign, callback) {
    redisWorkerManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
        if (err) {
            sqlManager.getWorker(tenantId, workerCallsign, (err, workerData) => {
                if (err || !workerData) {
                    return callback('No worker data');
                } else {
                    if (workerData && workerData.device && workerData.device_token) {
                        let workerDevice = workerData.device;
                        workerDevice = workerDevice.toLowerCase();
                        const workerDeviceToken = workerData.device_token;
                        const lang = workerData.lang;
                        const workerDeviceData = {
                            'device': workerDevice,
                            'device_token': workerDeviceToken,
                            'lang': lang
                        };
                        return callback(null, workerDeviceData);
                    }
                    return callback('Bad worker data');
                }
            });
        } else {
            if (workerData && workerData.worker && workerData.worker.device && workerData.worker.device_token) {
                let workerDevice = workerData.worker.device;
                workerDevice = workerDevice.toLowerCase();
                const workerDeviceToken = workerData.worker.device_token;
                const lang = workerData.worker.lang;
                const workerDeviceData = {
                    'device': workerDevice,
                    'device_token': workerDeviceToken,
                    'lang': lang
                };
                return callback(null, workerDeviceData);
            } else {
                return callback('Bad worker data');
            }
        }
    })
}


