"use strict";

const serviceLogger = require('../../services/logger').serviceLogger;
const pusher = require('../../services/pusher/pushService');

/**
 * Send push to worker  app with info about change balance of money
 * @param req
 * @param res
 * req.body
 *   tenant_id      68
 *   tenant_login   3colors
 *   balance        1000
 *   currency       RUB
 *   device         android
 *   token          12easdgdfgae12easdfasd
 *   lang           ru
 */
module.exports = function controller(req, res) {
    serviceLogger('info', "Called method: worker_send_push_transaction");
    serviceLogger('info', req.body);
    const answer = '{"result":1}';
    res.write(answer);
    res.end();
    const tenantId = parseInt(req.body.tenant_id);
    const tenantLogin = req.body.tenant_login;
    const balance = req.body.balance;
    const currency = req.body.currency;
    let typeDevice = req.body.device;
    const deviceToken = req.body.token;
    const lang = req.body.lang;
    req.i18n.setLocale(lang);
    const command = "new_balance";
    const contentAvailable = null;
    const actionLocKey = null;
    const category = null;
    const sound = 'default';
    const BalancePhrase = req.i18n.__("Your balance");
    const command_params = {
        'balance': balance,
        'currency': currency,
        'tenant_login': tenantLogin
    };
    const title = BalancePhrase + ' ' + balance + ' ' + currency;
    const settings = {
        title: title,
        message: title,
        contentAvailable: contentAvailable,
        actionLocKey: actionLocKey,
        category: category,
        icon: null,
        sound: sound,
        command: command,
        command_params: command_params,
    };
    if (typeof typeDevice === "string" && typeDevice.length > 0 && typeof deviceToken === "string" && deviceToken.length > 0) {
        typeDevice = typeDevice.toLowerCase();
        pusher.sendPush(tenantId, typeDevice, deviceToken, "worker", null, settings)
            .then(result => {
                serviceLogger('info', `${typeDevice} push result: ${JSON.stringify(result)}`);
            }).catch(err => {
            serviceLogger('error', `${typeDevice} push error: ${JSON.stringify(err)}`);
        });
    }
}


