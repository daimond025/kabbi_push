'use strict';

const serviceLogger = require('../../services/logger').serviceLogger;
const pusher = require('../../services/pusher/pushService');
const redisWorkerManager = require("../../dbs/redisWorkerManager");

/**
 * Send Push to worker on reject him from this order
 * TYPE REQUEST: GET
 * @param  req
 * req.body:
 *   order_id
 *   worker_callsign
 *   tenant_id
 *   status_id
 *@param  res
 */
module.exports = function controller(req, res) {
    serviceLogger('info', "Called method: reject_worker_from_order");
    serviceLogger('info', req.query);
    const answer = '{"result":1}';
    res.write(answer);
    res.end();
    const orderId = req.query.order_id;
    const workerCallsign = req.query.worker_callsign;
    const tenantId = req.query.tenant_id;
    const statusId = req.query.status_id;
    redisWorkerManager.getWorker(tenantId, workerCallsign, (err, worker) => {
        if (err) {
            serviceLogger('info', err);
        } else {
            let workerDevice = worker.worker.device;
            const workerDeviceToken = worker.worker.device_token;
            const lang = worker.worker.lang;
            req.i18n.setLocale(lang);
            const removedPhrase = req.i18n.__('You are removed from the order');
            const title = removedPhrase + " №" + orderId;
            const message = removedPhrase + " №" + orderId;
            const settings = {
                title: title,
                message: message,
                contentAvailable: null,
                actionLocKey: null,
                category: null,
                icon: null,
                sound: 'default',
                command: 'reject_worker_from_order',
                command_params: {
                    'order_id': orderId,
                    'status_id': statusId
                },

            };
            if (typeof workerDevice === "string" && workerDevice.length > 0 && typeof workerDeviceToken === "string" && workerDeviceToken.length > 0) {
                workerDevice = workerDevice.toLowerCase();
                pusher.sendPush(tenantId, workerDevice, workerDeviceToken, "worker",null, settings)
                    .then(result => {
                        serviceLogger('info', `${workerDevice} push result: ${JSON.stringify(result)}`);
                    }).catch(err => {
                    serviceLogger('error', `${workerDevice} push error: ${JSON.stringify(err)}`);
                });
            } else {
                serviceLogger('error', `BAD workerDevice or workerDeviceToken. typeDevice:${workerDevice}; deviceToken:${workerDeviceToken}`);
            }
        }
    })

};
